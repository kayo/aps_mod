EESchema Schematic File Version 2
LIBS:spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 0 #GND01
U 1 1 56F0D147
P 5000 3700
F 0 "#GND01" H 5000 3600 40  0001 C CNN
F 1 "0" H 5000 3630 40  0000 C CNN
F 2 "" H 5000 3700 60  0000 C CNN
F 3 "" H 5000 3700 60  0000 C CNN
	1    5000 3700
	1    0    0    -1  
$EndComp
$Comp
L OA XO1
U 1 1 56F0D779
P 5000 3300
F 0 "XO1" H 5100 3450 60  0000 C CNN
F 1 "LM258NS" V 5100 3000 60  0000 C CNN
F 2 "" H 4900 3300 60  0000 C CNN
F 3 "" H 4900 3300 60  0000 C CNN
	1    5000 3300
	1    0    0    -1  
$EndComp
Text GLabel 5000 2900 1    60   Input ~ 0
Vcc
$Comp
L R R3
U 1 1 56F0D85B
P 4200 4000
F 0 "R3" H 4200 4100 60  0000 C CNN
F 1 "3.3K" H 4200 3900 60  0000 C CNN
F 2 "" H 4200 4000 60  0000 C CNN
F 3 "" H 4200 4000 60  0000 C CNN
	1    4200 4000
	-1   0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 56F0D911
P 5000 4000
F 0 "R4" H 5000 4100 60  0000 C CNN
F 1 "100K" H 5000 3900 60  0000 C CNN
F 2 "" H 5000 4000 60  0000 C CNN
F 3 "" H 5000 4000 60  0000 C CNN
	1    5000 4000
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND02
U 1 1 56F0DA6C
P 2500 4100
F 0 "#GND02" H 2500 4000 40  0001 C CNN
F 1 "0" H 2500 4030 40  0000 C CNN
F 2 "" H 2500 4100 60  0000 C CNN
F 3 "" H 2500 4100 60  0000 C CNN
	1    2500 4100
	1    0    0    -1  
$EndComp
Text GLabel 3800 4000 0    60   Input ~ 0
IN1
$Comp
L V I1
U 1 1 56F0E0D9
P 3500 5700
F 0 "I1" H 3375 5900 60  0000 C CNN
F 1 "PWL(0 {Imin} 20mS {Imax} 40mS {Imin})" V 3725 5700 60  0000 C CNN
F 2 "" H 3175 5700 60  0000 C CNN
F 3 "" H 3175 5700 60  0000 C CNN
	1    3500 5700
	-1   0    0    1   
$EndComp
$Comp
L R R7
U 1 1 56F0E359
P 5600 5800
F 0 "R7" H 5600 5900 60  0000 C CNN
F 1 "{Rsh}" H 5600 5700 60  0000 C CNN
F 2 "" H 5600 5800 60  0000 C CNN
F 3 "" H 5600 5800 60  0000 C CNN
	1    5600 5800
	0    -1   1    0   
$EndComp
Text GLabel 5600 6800 3    60   Input ~ 0
IN1
Text Notes 7500 3700 0    60   ~ 0
+pspice\n* \n.control\n  options nopage\n  options savecurrents\n  tran 100uS 21mS 0mS\n  let Uref=v(REF)\n  let Uin=v(IN1,IN0)\n  let Uout=v(OUT)\n  let Iin=VI1#branch\n  set gnuplot_terminal=png\n  gnuplot project\n  + Uref\n  + Uin\n  + v(NEG)\n  + v(POS)\n  + Iin/10\n  + Uout\n  meas tran Iin0 find Iin at=20nS\n  meas tran Uout0 find Uout at=20nS\n  meas tran Iin1 find Iin at=10mS\n  meas tran Uout1 find Uout at=10mS\n  meas tran Iin2 find Iin at=20mS\n  meas tran Uout2 find Uout at=20mS\n.endc
$Comp
L V V1
U 1 1 56F0E8DC
P 2000 5700
F 0 "V1" H 1875 5900 60  0000 C CNN
F 1 "DC 5V" V 2225 5700 60  0000 C CNN
F 2 "" H 1675 5700 60  0000 C CNN
F 3 "" H 1675 5700 60  0000 C CNN
	1    2000 5700
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND03
U 1 1 56F0E9E4
P 2000 6000
F 0 "#GND03" H 2000 5900 40  0001 C CNN
F 1 "0" H 2000 5930 40  0000 C CNN
F 2 "" H 2000 6000 60  0000 C CNN
F 3 "" H 2000 6000 60  0000 C CNN
	1    2000 6000
	1    0    0    -1  
$EndComp
Text GLabel 2000 5400 1    60   Input ~ 0
Vcc
Text GLabel 4600 3400 0    60   Input ~ 0
NEG
$Comp
L R Ra6
U 1 1 56F12BE7
P 2500 2800
F 0 "Ra6" H 2500 2900 60  0000 C CNN
F 1 "8.2K" H 2500 2700 60  0000 C CNN
F 2 "" H 2500 2800 60  0000 C CNN
F 3 "" H 2500 2800 60  0000 C CNN
	1    2500 2800
	0    -1   -1   0   
$EndComp
$Comp
L R Ra5
U 1 1 56F12C6C
P 2500 3700
F 0 "Ra5" H 2500 3800 60  0000 C CNN
F 1 "330" H 2500 3600 60  0000 C CNN
F 2 "" H 2500 3700 60  0000 C CNN
F 3 "" H 2500 3700 60  0000 C CNN
	1    2500 3700
	0    -1   -1   0   
$EndComp
Text GLabel 2500 2400 1    60   Input ~ 0
Vcc
Text GLabel 4600 3200 0    60   Input ~ 0
POS
Text Notes 800  3000 0    60   ~ 0
Measurement\nRanges:\n(see: range.max)\n\nI: 0 .. 40 A\ndI: 13.25 A/V\n\nU: 0.26 .. 3.28 V\ndU: 75.5 mV/A\n\nA: 24 .. 3784 units\ndA: 94 units/A\n\ndIdA: ~10 mA/unit
$Comp
L R Ro1
U 1 1 57D15242
P 6200 3600
F 0 "Ro1" H 6200 3700 60  0000 C CNN
F 1 "10Meg" H 6200 3500 60  0000 C CNN
F 2 "" H 6200 3600 60  0000 C CNN
F 3 "" H 6200 3600 60  0000 C CNN
	1    6200 3600
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND04
U 1 1 57D152C4
P 6200 4000
F 0 "#GND04" H 6200 3900 40  0001 C CNN
F 1 "0" H 6200 3930 40  0000 C CNN
F 2 "" H 6200 4000 60  0000 C CNN
F 3 "" H 6200 4000 60  0000 C CNN
	1    6200 4000
	1    0    0    -1  
$EndComp
Text GLabel 6200 3200 1    60   Input ~ 0
OUT
Text Notes 3400 1700 0    60   ~ 0
-pspice\n* \n.include ../.spice/op-amp.lib
Text GLabel 5500 3300 2    60   Input ~ 0
OUT
Text Notes 7850 5300 0    60   ~ 0
-pspice\n* \n.param\n+ Imin=0A\n+ Imax=40A\n+ Rsh=10m/4\n+ Rgw=1m\n+ Rpw=1m\n+ Rld=0
Text GLabel 3500 6100 3    60   Input ~ 0
IN1
Text GLabel 3500 5300 1    60   Input ~ 0
IN
$Comp
L 0 #GND05
U 1 1 58BFD83C
P 4900 6200
F 0 "#GND05" H 4900 6100 40  0001 C CNN
F 1 "0" H 4900 6130 40  0000 C CNN
F 2 "" H 4900 6200 60  0000 C CNN
F 3 "" H 4900 6200 60  0000 C CNN
	1    4900 6200
	1    0    0    -1  
$EndComp
$Comp
L V VI1
U 1 1 58BFDA58
P 5600 6400
F 0 "VI1" H 5475 6600 60  0000 C CNN
F 1 "DC 0V" V 5825 6400 60  0000 C CNN
F 2 "" H 5275 6400 60  0000 C CNN
F 3 "" H 5275 6400 60  0000 C CNN
	1    5600 6400
	1    0    0    -1  
$EndComp
Text Notes 4100 4250 0    60   ~ 0
R[n]
Text Notes 4900 4250 0    60   ~ 0
R[fb]
Text Notes 5500 3200 0    60   ~ 0
U[o]
Text Notes 3600 3900 0    60   ~ 0
U[n]
Text Notes 2600 2350 0    60   ~ 0
U[s]
Text Notes 2100 3700 0    60   ~ 0
R[rl]
Text Notes 2100 2850 0    60   ~ 0
R[rh]
Text GLabel 2700 3200 2    60   Input ~ 0
REF
$Comp
L R R2
U 1 1 58BFFFD6
P 5000 2500
F 0 "R2" H 5000 2600 60  0000 C CNN
F 1 "100K" H 5000 2400 60  0000 C CNN
F 2 "" H 5000 2500 60  0000 C CNN
F 3 "" H 5000 2500 60  0000 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
Text Notes 3600 2400 0    60   ~ 0
U[p]
Text Notes 4100 2300 0    60   ~ 0
R[p]
Text Notes 4900 2300 0    60   ~ 0
R[pu]
Text GLabel 5400 2500 2    60   Input ~ 0
REF
$Comp
L R R1
U 1 1 58BFFFDC
P 4200 2500
F 0 "R1" H 4200 2600 60  0000 C CNN
F 1 "3.3K" H 4200 2400 60  0000 C CNN
F 2 "" H 4200 2500 60  0000 C CNN
F 3 "" H 4200 2500 60  0000 C CNN
	1    4200 2500
	1    0    0    -1  
$EndComp
Text GLabel 3800 2500 0    60   Input ~ 0
IN0
Text GLabel 5600 5400 1    60   Input ~ 0
IN0
Text GLabel 4900 5400 1    60   Input ~ 0
IN0
$Comp
L R R6
U 1 1 58C00FAA
P 4900 5800
F 0 "R6" H 4900 5900 60  0000 C CNN
F 1 "{Rgw}" H 4900 5700 60  0000 C CNN
F 2 "" H 4900 5800 60  0000 C CNN
F 3 "" H 4900 5800 60  0000 C CNN
	1    4900 5800
	0    -1   1    0   
$EndComp
Text Notes 5400 2400 0    60   ~ 0
U[r]
Text Notes 5150 5950 1    60   ~ 0
R[wr]
Text Notes 4750 6000 1    60   ~ 0
gnd wire
Text Notes 3750 6000 1    60   ~ 0
power supply
Text Notes 5850 5950 1    60   ~ 0
R[sh]
Text Notes 5450 5950 1    60   ~ 0
shunt
Text Notes 5400 6650 1    60   ~ 0
ampermeter
Wire Wire Line
	5000 3600 5000 3700
Wire Wire Line
	5000 3000 5000 2900
Wire Wire Line
	4700 3400 4600 3400
Wire Wire Line
	4600 3400 4600 4000
Wire Wire Line
	4400 4000 4800 4000
Connection ~ 4600 4000
Wire Wire Line
	5400 3300 5400 4000
Wire Wire Line
	5400 4000 5200 4000
Wire Wire Line
	2000 5900 2000 6000
Wire Wire Line
	2000 5400 2000 5500
Wire Wire Line
	4000 4000 3800 4000
Wire Wire Line
	2700 3200 2500 3200
Wire Wire Line
	2500 3000 2500 3500
Connection ~ 2500 3200
Wire Wire Line
	2500 3900 2500 4100
Wire Wire Line
	2500 2600 2500 2400
Connection ~ 5400 3300
Wire Wire Line
	6200 3200 6200 3400
Wire Wire Line
	6200 3800 6200 4000
Wire Wire Line
	5300 3300 5500 3300
Wire Wire Line
	3500 5900 3500 6100
Wire Wire Line
	3500 5300 3500 5500
Wire Wire Line
	5600 6800 5600 6600
Wire Wire Line
	5600 5600 5600 5400
Wire Wire Line
	5600 6200 5600 6000
Wire Wire Line
	4600 3200 4700 3200
Wire Wire Line
	4000 2500 3800 2500
Wire Wire Line
	5200 2500 5400 2500
Wire Wire Line
	4900 5400 4900 5600
Wire Wire Line
	4900 6000 4900 6200
Wire Wire Line
	4600 3200 4600 2500
Wire Wire Line
	4400 2500 4800 2500
Connection ~ 4600 2500
Wire Notes Line
	3500 6400 3500 7200
Wire Notes Line
	3500 7200 5600 7200
Wire Notes Line
	5600 7200 5600 7100
Wire Notes Line
	3500 5100 3500 5000
Wire Notes Line
	3500 5000 4200 5000
Wire Notes Line
	4200 5000 4200 5200
Wire Notes Line
	4200 6400 4200 6500
Wire Notes Line
	4200 6500 4900 6500
Wire Notes Line
	4900 6500 4900 6400
Wire Notes Line
	4900 5100 4900 5000
Wire Notes Line
	4900 5000 5600 5000
Wire Notes Line
	5600 5000 5600 5100
Text Notes 4500 2450 0    60   ~ 0
U[b]
Text Notes 3650 3050 0    60   ~ 0
U[b] > 5mV
Text Notes 4250 4450 0    60   ~ 0
see: project.max
Wire Wire Line
	4200 6000 4200 6200
Wire Wire Line
	4200 5400 4200 5600
Text Notes 4050 6150 1    60   ~ 0
load & pwr wire
Text GLabel 4200 5400 1    60   Input ~ 0
IN
$Comp
L 0 #GND06
U 1 1 56F0E4C5
P 4200 6200
F 0 "#GND06" H 4200 6100 40  0001 C CNN
F 1 "0" H 4200 6130 40  0000 C CNN
F 2 "" H 4200 6200 60  0000 C CNN
F 3 "" H 4200 6200 60  0000 C CNN
	1    4200 6200
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 56F0E276
P 4200 5800
F 0 "R5" H 4200 5900 60  0000 C CNN
F 1 "{Rpw+Rld}" H 4200 5700 60  0000 C CNN
F 2 "" H 4200 5800 60  0000 C CNN
F 3 "" H 4200 5800 60  0000 C CNN
	1    4200 5800
	0    -1   1    0   
$EndComp
$EndSCHEMATC
