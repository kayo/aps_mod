#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/sync.h>

#include <stddef.h>
#include <stdbool.h>

#include "macro.h"
#include "systick.h"
#include "monitor.h"
#include "panel.h"
#include "config.h"
#include "ticks.h"
#include "evt.h"

#include "delay.h"
#include "ctl/ewma.h"
#include "ctl/util.h"
#include "ctl/pid.h"

#if USE_PARAMS
#include "params.h"
#endif

#include "sensor.h"
#include "driver.h"

#if USE_RS485 || USE_RADIO
#include "console.h"
#endif

#if USE_RS485
console_def(rs485);
#endif

#if USE_RADIO
console_def(radio);
#endif

#include "fan.h"

#ifndef FAST_INIT
#define FAST_INIT 0
#endif

static void init(void) {
  /* Setup MCU clock */
  _CAT3(rcc_clock_setup_in_hse_8mhz_out_, mcu_frequency, mhz)();

#if FAST_INIT
  rcc_peripheral_enable_clock(&RCC_AHBENR, RCC_AHBENR_DMA1EN);
  rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_DACEN);
  rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPDEN | RCC_APB2ENR_AFIOEN | RCC_APB2ENR_ADC1EN | RCC_APB2ENR_USART1EN);
#else
  /* Enable GPIOA clock */
	rcc_periph_clock_enable(RCC_GPIOA);
  
  /* Enable GPIOB clock */
	rcc_periph_clock_enable(RCC_GPIOB);

  /* Enable AFIO clock */
	rcc_periph_clock_enable(RCC_AFIO);

  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);
#endif

  /* Disable JTAG only SWD, USE PD01 as GPIO */
  gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, AFIO_MAPR_PD01_REMAP);
  
  sensor_init();
  driver_init();
  fan_init();
  panel_init();
  
#if USE_RS485
  rs485_init();
#endif

#if USE_RADIO
  radio_init();
#endif
  
	systick_init();
}

static void done(void) {
  systick_done();

#if USE_RADIO
  radio_done();
#endif

#if USE_RS485
  rs485_done();
#endif

  panel_done();
  driver_done();
  sensor_done();
  fan_done();
  
  /* Deconfigure all GPIOs */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  
#if FAST_INIT
  rcc_peripheral_disable_clock(&RCC_AHBENR, RCC_AHBENR_DMA1EN);
  rcc_peripheral_disable_clock(&RCC_APB1ENR, RCC_APB1ENR_DACEN);
  rcc_peripheral_disable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPDEN | RCC_APB2ENR_AFIOEN | RCC_APB2ENR_ADC1EN | RCC_APB2ENR_USART1EN);
#else
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);

  /* Disable AFIO clock */
	rcc_periph_clock_disable(RCC_AFIO);

  /* Disable GPIOB clock */
	rcc_periph_clock_disable(RCC_GPIOB);

  /* Disable GPIOA clock */
	rcc_periph_clock_disable(RCC_GPIOA);
#endif
}

/* main control */

monitor_def();

static ticks_t ticks = 0;

ticks_t ticks_read(void) {
  return ticks;
}

#if USE_MEMCHECK
static mem_info_t mem_info;
#endif

void sensor_step(void) {
  //driver_step();
  fan_step();
}

int main(void) {
#if USE_MEMCHECK
  mem_prefill();
#endif
  
  init();
  
  param_init(&params);
  monitor_init();

  for (;; ticks += 100) {
    monitor_wait();

    if (!(ticks % 1000)) {
      evt_push(evt_tick);
    }

    /* Handle UART communication */
#if USE_RS485
    rs485_step(&params);
#endif

#if USE_RADIO
    radio_step(&params);
#endif

    panel_step();

#if USE_MEMCHECK
    mem_measure(&mem_info);
#endif
  }
  
  done();
  
  return 0;
}
