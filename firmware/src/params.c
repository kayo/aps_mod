#include "params.h"

#include "config.h"
#include "macro.h"

#define ctl_period fix16_make(_NTH2(sensor_timer)*1e-3)

#include "sensor.h"
#include "fan.h"
#include "driver.h"

static int Tfti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Tfti;
  
  return param_success;
}

static int Tfti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Tfti = *(const fix16*)value;
  _CAT2(TEMP_FILTER, _set_T)(fix16, &temp_filter_param, Tfti, ctl_period);
  
  return param_success;
}

static int Ifti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ifti;
  
  return param_success;
}

static int Ifti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ifti = *(const fix16*)value;
  _CAT2(CURR_FILTER, _set_T)(fix16, &curr_filter_param, Ifti, ctl_period);
  
  return param_success;
}

static int Ufti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ufti;
  
  return param_success;
}

static int Ufti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ufti = *(const fix16*)value;
  _CAT2(VOLT_FILTER, _set_T)(fix16, &volt_filter_param, Ufti, ctl_period);
  
  return param_success;
}

static int Ipid_Kp_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ipid_Kp;

  return param_success;
}

static int Ipid_Kp_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ipid_Kp = *(const fix16*)value;
  pid_set_Kp(fix16, &Ipid_param, Ipid_Kp);
  
  return param_success;
}

static int Ipid_Ti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ipid_Ti;

  return param_success;
}

static int Ipid_Ti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ipid_Ti = *(const fix16*)value;
  pid_set_Ti(fix16, &Ipid_param, Ipid_Ti, ctl_period);
  
  return param_success;
}

static int Ipid_Td_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ipid_Td;

  return param_success;
}

static int Ipid_Td_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ipid_Td = *(const fix16*)value;
  pid_set_Td(fix16, &Ipid_param, Ipid_Td, ctl_period);
  
  return param_success;
}

static int Ipid_Ei_lim_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Ipid_Ei_lim;

  return param_success;
}

static int Ipid_Ei_lim_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Ipid_Ei_lim = *(const fix16*)value;
  pid_set_Ei_limit(fix16, &Ipid_param, Ipid_Ei_lim);
  
  return param_success;
}

static int Upid_Kp_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Upid_Kp;

  return param_success;
}

static int Upid_Kp_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Upid_Kp = *(const fix16*)value;
  pid_set_Kp(fix16, &Upid_param, Upid_Kp);
  
  return param_success;
}

static int Upid_Ti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Upid_Ti;

  return param_success;
}

static int Upid_Ti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Upid_Ti = *(const fix16*)value;
  pid_set_Ti(fix16, &Upid_param, Upid_Ti, ctl_period);
  
  return param_success;
}

static int Upid_Td_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Upid_Td;

  return param_success;
}

static int Upid_Td_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Upid_Td = *(const fix16*)value;
  pid_set_Td(fix16, &Upid_param, Upid_Td, ctl_period);
  
  return param_success;
}

static int Upid_Ei_lim_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Upid_Ei_lim;

  return param_success;
}

static int Upid_Ei_lim_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Upid_Ei_lim = *(const fix16*)value;
  pid_set_Ei_limit(fix16, &Upid_param, Upid_Ei_lim);
  
  return param_success;
}

static int Tpid_Kp_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Tpid_Kp;

  return param_success;
}

static int Tpid_Kp_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Tpid_Kp = *(const fix16*)value;
  pid_set_Kp(fix16, &Tpid_param, Tpid_Kp);
  
  return param_success;
}

static int Tpid_Ti_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Tpid_Ti;

  return param_success;
}

static int Tpid_Ti_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Tpid_Ti = *(const fix16*)value;
  pid_set_Ti(fix16, &Tpid_param, Tpid_Ti, ctl_period);
  
  return param_success;
}

static int Tpid_Td_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Tpid_Td;

  return param_success;
}

static int Tpid_Td_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Tpid_Td = *(const fix16*)value;
  pid_set_Td(fix16, &Tpid_param, Tpid_Td, ctl_period);
  
  return param_success;
}

static int Tpid_Ei_lim_get(const param_desc_t *param, void *value) {
  (void)param;
  
  *(fix16*)value = Tpid_Ei_lim;

  return param_success;
}

static int Tpid_Ei_lim_set(const param_desc_t *param, const void *value) {
  (void)param;
  
  Tpid_Ei_lim = *(const fix16*)value;
  pid_set_Ei_limit(fix16, &Tpid_param, Tpid_Ei_lim);
  
  return param_success;
}

#define __params_c__
#include PARAMS_params_H
