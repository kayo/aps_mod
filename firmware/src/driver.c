#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/dac.h>
#include <libopencm3/stm32/timer.h>

#include "driver.h"

#include "params.h"
#include "ctl/util.h"

#include "macro.h"
#include "config.h"

#define DRV_CFG driver_pwm
/* pwm timer number */
#define DRV_NUM _NTH0(DRV_CFG)
/* pwm frequency in Hz */
#define DRV_FHZ (_NTH1(DRV_CFG)*1e3*2)
/* pwm dead-time in S */
#define DRV_DTS (_NTH2(DRV_CFG)*1e-6/2)

#define DRV_RCC _CAT2(RCC_TIM, DRV_NUM)
#define DRV_DEV _CAT2(TIM, DRV_NUM)

#define ROUND(f) (((uint32_t)((f)*2.0)+1)>>1)

/* timer prescaler */
#define DRV_PSC ROUND((mcu_frequency)*1e6/DRV_FHZ/(1<<16))
/* timer divider */
#define DRV_DIV ((DRV_PSC)+1)
/* timer period in ticks */
#define DRV_PER ROUND((mcu_frequency)*1e6/DRV_FHZ/DRV_DIV)
/* timer dead-time in ticks */
#define DRV_DTC ROUND((mcu_frequency)*1e6/DRV_DIV*DRV_DTS)
/* timer max duty cycle in ticks */
#define DRV_TOP (DRV_PER/2-DRV_DTC)

#define HIK_CFG hikey_output
#define HIK_PORT _CAT2(GPIO, _NTH0(HIK_CFG))
#define HIK_PAD _CAT2(GPIO, _NTH1(HIK_CFG))
#define HIK_POL _NTH2(HIK_CFG)
#define HIK_OC _CAT2(TIM_OC, _NTH3(HIK_CFG))

#define LOK_CFG lokey_output
#define LOK_PORT _CAT2(GPIO, _NTH0(LOK_CFG))
#define LOK_PAD _CAT2(GPIO, _NTH1(LOK_CFG))
#define LOK_POL _NTH2(HIK_CFG)
#define LOK_OC _CAT2(TIM_OC, _NTH3(LOK_CFG))

driver_pid_param_t Ipid_param;
driver_pid_param_t Upid_param;

#define driver_pid_state_s pid_state_t(fix16)
typedef driver_pid_state_s driver_pid_state_t;

static driver_pid_state_t Ipid_state;
static driver_pid_state_t Upid_state;

static void _pwm_oc_config(uint32_t timer, enum tim_oc_id oc_id, char pol){
  /* Disable output */
  timer_disable_oc_output(timer, oc_id);

  /* Configure global mode of line */
  timer_disable_oc_clear(timer, oc_id);
  timer_enable_oc_preload(timer, oc_id);
  timer_set_oc_slow_mode(timer, oc_id);
  timer_set_oc_mode(timer, oc_id, TIM_OCM_PWM1);
  
  /* Configure OC */
  if (pol) {
    timer_set_oc_polarity_high(timer, oc_id);
		timer_set_oc_idle_state_unset(timer, oc_id);
  } else {
		timer_set_oc_polarity_low(timer, oc_id);
		timer_set_oc_idle_state_set(timer, oc_id);
  }
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(timer, oc_id, 0);
  
  /* Enable output */
  timer_enable_oc_output(timer, oc_id);
}

static void _pwm_oc_deconf(uint32_t timer, enum tim_oc_id oc_id){
  /* Disable output */
  timer_disable_oc_output(timer, oc_id);
}

void driver_init(void){
	pid_init(fix16, &Ipid_state);
	pid_init(fix16, &Upid_state);
	
  /* Enable TIM clock */
  rcc_periph_clock_enable(DRV_RCC);

#if HIK_PORT == LOK_PORT
  gpio_set_mode(HIK_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, HIK_PAD | LOK_PAD);
#else
  gpio_set_mode(HIK_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, HIK_PAD);
  gpio_set_mode(LOK_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, LOK_PAD);
#endif

  /* Reset TIM peripheral */
  timer_reset(DRV_DEV);

  /* Timer global mode:
   * - No divider
   * - Alignment edge
   * - Direction up
   */
  timer_set_mode(DRV_DEV, TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

  /* Reset prescaler value */
  timer_set_prescaler(DRV_DEV, DRV_PSC);

  /* Enable preload */
  timer_enable_preload(DRV_DEV);

  /* Continuous mode */
  timer_continuous_mode(DRV_DEV);

	/* Center-aligned mode */
  timer_set_alignment(DRV_DEV, TIM_CR1_CMS_CENTER_3);
  
  /* Period */
  timer_set_period(DRV_DEV, DRV_PER);

# if 0
  /* Configure break and deadtime */
  timer_set_enabled_off_state_in_idle_mode(DRV_DEV);
  timer_set_enabled_off_state_in_run_mode(DRV_DEV);
  timer_disable_break(DRV_DEV);
  timer_set_break_polarity_high(DRV_DEV);
  timer_disable_break_automatic_output(DRV_DEV);
  timer_set_break_lock(DRV_DEV, TIM_BDTR_LOCK_OFF);
#endif
  
  _pwm_oc_config(DRV_DEV, HIK_OC, HIK_POL);
  _pwm_oc_config(DRV_DEV, LOK_OC, !LOK_POL);
  
  /* ARR reload enable. */
  timer_enable_preload(DRV_DEV);
  
  /* Enable outputs in the break subsystem. */
  timer_enable_break_main_output(DRV_DEV);
  
  /* Counter enable. */
  timer_enable_counter(DRV_DEV);
}

void driver_done(void){
  _pwm_oc_deconf(DRV_DEV, HIK_OC);
  _pwm_oc_deconf(DRV_DEV, LOK_OC);
  
  /* Counter disable */
  timer_disable_counter(DRV_DEV);

  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO6 | GPIO7);

#if HIK_PORT == LOK_PORT
  gpio_set_mode(HIK_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, HIK_PAD | LOK_PAD);
#else
  gpio_set_mode(HIK_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, HIK_PAD);
  gpio_set_mode(LOK_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, LOK_PAD);
#endif
  
  /* Disable DRV_DEV clock */
  rcc_periph_clock_disable(DRV_RCC);
}

fix16 Idrv, Udrv;

void driver_step(void){
	Ierr = error_val(fix16, Itar, Isen);
	Uerr = error_val(fix16, Utar, Usen);
	
	Idrv = pid_step(fix16, &Ipid_param, &Ipid_state, Ierr);
	Udrv = pid_step(fix16, &Upid_param, &Upid_state, Uerr);
	
	driver_power = clamp_val_const(fix16,
																 min_val(fix16, Idrv, Udrv),
																 0.0, 1.0);
	
  uint16_t duty = math_op(fix16, int,
													math_op(fix16, mul,
																	math_op(fix16, make, DRV_TOP),
																	driver_power));
  
  timer_set_oc_value(DRV_DEV, HIK_OC, duty);
  timer_set_oc_value(DRV_DEV, LOK_OC, DRV_PER - duty);
}
