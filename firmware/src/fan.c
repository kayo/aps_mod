#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "fan.h"

#include "params.h"
#include "ctl/util.h"

#include "macro.h"
#include "config.h"

#define FAN_RCC _CAT2(RCC_TIM, _NTH0(fan_timer))
#define FAN_DEV _CAT2(TIM, _NTH0(fan_timer))

/* timer frequency in Hz */
#define FAN_FHZ (_NTH1(fan_timer)*1e3)
#define FAN_INIT _NTH2(fan_timer)

#define ROUND(f) (((uint32_t)((f)*2.0)+1)>>1)

/* timer prescaler */
#define FAN_PSC ROUND((mcu_frequency)*1e6/FAN_FHZ/(1<<16))
/* timer divider */
#define FAN_DIV ((FAN_PSC)+1)
/* timer period in ticks */
#define FAN_PER ROUND((mcu_frequency)*1e6/FAN_FHZ/FAN_DIV)
/* timer max duty cycle in ticks */
#define FAN_TOP (FAN_PER-1)

#define FAN_PORT _CAT2(GPIO, _NTH0(fan_output))
#define FAN_PAD _CAT2(GPIO, _NTH1(fan_output))
#define FAN_POL _CAT2(GPIO, _NTH2(fan_output))
#define FAN_OC _CAT2(TIM_OC, _NTH3(fan_output))

fan_pid_param_t Tpid_param;

#define fan_pid_state_s pid_state_t(fix16)
typedef fan_pid_state_s fan_pid_state_t;

static fan_pid_state_t Tpid_state;

static inline void pwm_oc_enable(uint32_t timer, enum tim_oc_id oc_id, uint8_t pol) {
  /* Disable output */
  timer_disable_oc_output(timer, oc_id);

  /* Configure global mode of line */
  timer_disable_oc_clear(timer, oc_id);
  timer_enable_oc_preload(timer, oc_id);
  timer_set_oc_slow_mode(timer, oc_id);
  timer_set_oc_mode(timer, oc_id, TIM_OCM_PWM1);
  
  /* Configure OC */
  if (pol) {
	timer_set_oc_polarity_high(timer, oc_id);
	timer_set_oc_idle_state_unset(timer, oc_id);
  } else {
	timer_set_oc_polarity_low(timer, oc_id);
	timer_set_oc_idle_state_set(timer, oc_id);
  }
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(timer, oc_id, 0);
  
  timer_set_oc_polarity_high(timer, oc_id);
  timer_set_oc_idle_state_unset(timer, oc_id);
  
  /* Enable output */
  timer_enable_oc_output(timer, oc_id);
}

static inline void pwm_oc_disable(uint32_t timer, enum tim_oc_id oc_id) {
  /* Disable output */
  timer_disable_oc_output(timer, oc_id);
}

void fan_init(void) {
  pid_init(fix16, &Tpid_state);

#if FAN_INIT
  /* Enable TIM clock */
  rcc_periph_clock_enable(FAN_RCC);
#endif /* FAN_INIT */
  
  gpio_set_mode(FAN_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, FAN_PAD);

#if FAN_INIT
  /* Reset TIM peripheral */
  timer_reset(FAN_DEV);

  /* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 */
  timer_set_mode(FAN_DEV, TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

  /* Reset prescaler value */
  timer_set_prescaler(FAN_DEV, FAN_DIV);

  /* Enable preload */
  timer_enable_preload(FAN_DEV);

  /* Continuous mode */
  timer_continuous_mode(FAN_DEV);
  
  /* Period */
  timer_set_period(FAN_DEV, FAN_MAX);
  
  /* Configure break and deadtime */
  timer_set_enabled_off_state_in_idle_mode(FAN_DEV);
  timer_set_enabled_off_state_in_run_mode(FAN_DEV);
  timer_disable_break(FAN_DEV);
  timer_set_break_polarity_high(FAN_DEV);
  timer_disable_break_automatic_output(FAN_DEV);
  timer_set_break_lock(FAN_DEV, TIM_BDTR_LOCK_OFF);
#endif /* FAN_INIT */
  
  pwm_oc_enable(FAN_DEV, FAN_OC, FAN_POL);

#if FAN_INIT
  /* ARR reload enable. */
  timer_enable_preload(FAN_DEV);
  
  /*
   * Enable preload of complementary channel configurations and
   * update on COM event.
   */
  timer_enable_preload_complementry_enable_bits(FAN_DEV);
  
  /* Enable outputs in the break subsystem. */
  timer_enable_break_main_output(FAN_DEV);
  
  /* Counter enable. */
  timer_enable_counter(FAN_DEV);
#endif /* FAN_INIT */
}

void fan_done(void) {
  pwm_oc_disable(FAN_DEV, FAN_OC);

#ifdef FAN_INIT
  /* Counter disable */
  timer_disable_counter(FAN_DEV);
  
  /* Disable FAN_DEV clock */
  rcc_periph_clock_disable(FAN_RCC);
#endif /* FAN_INIT */

  gpio_set_mode(FAN_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, FAN_PAD);
}

fix16 fan_power_raw;

void fan_step(void) {
  Terr = max_val(fix16,
				 error_val(fix16, Tnom, Ths1),
				 error_val(fix16, Tnom, Ths2));
  
  fan_power_raw = pid_step(fix16, &Tpid_param, &Tpid_state, Terr);
  
  fan_power = clamp_val_const(fix16, fan_power_raw, 0.0, 1.0);
  
  timer_set_oc_value(FAN_DEV, FAN_OC,
					 math_op(fix16, int,
							 math_op(fix16, mul,
									 math_op(fix16, make, FAN_TOP),
									 fan_power)));
}
