#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include <stdlib.h>
#include <math.h>

#include "ctl/util.h"

#include "sensor.h"
#include "params.h"
#include "macro.h"
#include "config.h"
#include "lookup.h"

#if !defined(HANNEL_VREF) && defined(ADC_CHANNEL_VREFINT)
#define ADC_CHANNEL_VREF ADC_CHANNEL_VREFINT
#endif

/* hardware defines */
#ifdef temp1_input
#define TEMP1 temp1_input
#define TEMP1_PORT _CAT2(GPIO, _NTH0(TEMP1))
#define TEMP1_PAD _CAT2(GPIO, _NTH1(TEMP1))
#define TEMP1_CH _CAT2(ADC_CHANNEL, _NTH2(TEMP1))
#define TEMP1_FLAG 0
#endif

#ifdef temp2_input
#define TEMP2 temp2_input
#define TEMP2_PORT _CAT2(GPIO, _NTH0(TEMP2))
#define TEMP2_PAD _CAT2(GPIO, _NTH1(TEMP2))
#define TEMP2_CH _CAT2(ADC_CHANNEL, _NTH2(TEMP2))
#define TEMP2_FLAG 1
#endif

#ifdef curr_input
#define CURR_PORT _CAT2(GPIO, _NTH0(curr_input))
#define CURR_PAD _CAT2(GPIO, _NTH1(curr_input))
#define CURR_CH _CAT2(ADC_CHANNEL, _NTH2(curr_input))
#define CURR_FLAG 2
#endif

#ifdef volt_input
#define VOLT_PORT _CAT2(GPIO, _NTH0(volt_input))
#define VOLT_PAD _CAT2(GPIO, _NTH1(volt_input))
#define VOLT_CH _CAT2(ADC_CHANNEL, _NTH2(volt_input))
#define VOLT_FLAG 3
#endif

#ifdef sensor_testout
#define TEST sensor_testout
#define TEST_PORT _CAT2(GPIO, _NTH0(TEST))
#define TEST_PAD _CAT2(GPIO, _NTH1(TEST))
#endif

#define _ADC1 ADC1

#define ADC_RCC _CAT2(RCC_ADC, _NTH0(sensor_adc))
#define ADC_DEV _CAT2(_ADC, _NTH0(sensor_adc))
#define ADC_BITS _NTH1(sensor_adc)
#define ADC_PRE _CAT2(RCC_CFGR_ADCPRE_PCLK2_DIV, _NTH2(sensor_adc))

#ifdef sensor_timer
#define ADC_TRIG _CAT4(ADC_CR2_EXTSEL_TIM, _NTH0(sensor_timer), _CC, _NTH1(sensor_timer))

#define TRIG_RCC _CAT2(RCC_TIM, _NTH0(sensor_timer))
#define TRIG_DEV _CAT2(TIM, _NTH0(sensor_timer))
#define TRIG_OC _CAT2(TIM_OC, _NTH1(sensor_timer))
#define TRIG_MMS _CAT3(TIM_CR2_MMS_COMPARE_OC, _NTH1(sensor_timer), REF)
#define TRIG_PER (_NTH2(sensor_timer)*1e-3) /* period (S) */

#define ROUND(x) ((((x)*2)+1)/2)

#define TRIG_PSC ((uint16_t)(TRIG_PER*((mcu_frequency)*1e6)/(1<<16))) /* prescaler */
#define TRIG_DIV (TRIG_PSC+1) /* divider */
#define TRIG_TOP ((uint16_t)ROUND(TRIG_PER*((mcu_frequency)*1e6)/TRIG_DIV)) /* counter limit */

#else
//#define ADC_TRIG ADC_CR2_EXTSEL_SWSTART
#endif

#define ADC_VOLT _UNWR(_NTH3(sensor_adc))
#define ADC_VREF _NTH0(ADC_VOLT)
#define ADC_VTOP _NTH1(ADC_VOLT)

#define ADC_DMA _CAT2(DMA, _NTH0(sensor_dma))
#define ADC_DMA_CH _CAT2(DMA_CHANNEL, _NTH1(sensor_dma))
#define ADC_DMA_IRQ _CAT5(NVIC_DMA, _NTH0(sensor_dma), _CHANNEL, _NTH1(sensor_dma), _IRQ)
#define adc_dma_isr _CAT5(dma, _NTH0(sensor_dma), _channel, _NTH1(sensor_dma), _isr)

#ifdef tmcu_config
#define TMCU tmcu_config
#define TMCU_TEMPER_T0 _NTH0(TMCU)
#define TMCU_VSENSE_T0 _NTH1(TMCU)
#define TMCU_AVG_SLOPE _NTH2(TMCU)
#endif

#ifdef vref_config
#define VREF vref_config
#endif

#ifdef therm_config
#define THERM_RESISTORS _UNWR(_NTH0(therm_config))
#define THERM_MODEL _NTH1(therm_config)
#define THERM_PARAMS _UNWR(_NTH2(therm_config))
#endif

/* ADC filtering and conversion */
#ifdef filter_config
#define ADC_SAMPLES _NTH0(filter_config)
#define ADC_SELECT _NTH1(filter_config)
#else
#define ADC_SAMPLES 1
#define ADC_SELECT 0
#endif

#define ADC_OVER ((adc_value_t)(1 << ADC_BITS))
#define ADC_TOP ((adc_value_t)(ADC_OVER - 1))
#define ADC_UV ((float)ADC_TOP / (float)ADC_VTOP)
#define ADC_REF ((adc_value_t)(ADC_VREF * ADC_UV))

typedef uint16_t adc_value_t;

typedef struct {
  adc_value_t Tmcu;
  adc_value_t Uref;
  adc_value_t Isen;
  adc_value_t Usen;
#ifdef TEMP1
  adc_value_t Ths1;
#endif
#ifdef TEMP2
  adc_value_t Ths2;
#endif
} adc_values_t;

static adc_values_t adc_buffer[ADC_SAMPLES];
static uint8_t filter_init = 0;
#define filter_inited(flag) (((filter_init) & (1<<(flag))) || ((filter_init |= (1<<(flag))) && 0))

#define ADC_CHANNELS (sizeof(adc_values_t)/sizeof(adc_value_t))

#if ADC_SELECT > 0
static int adc_comp(const void *a, const void *b) {
  return (const adc_value_t*)a < (const adc_value_t*)b ? -1 : (const adc_value_t*)a > (const adc_value_t*)b ? 1 : 0;
}
#endif

static adc_value_t adc_value_sample(const adc_value_t *field) {
#if ADC_SELECT > 0 /* have median filter */
  adc_value_t values[ADC_SAMPLES], *value = values, *end = values + ADC_SAMPLES;
  
  for(; value < end; ){
    *value++ = *field;
    field += ADC_CHANNELS;
  }
  
  qsort(values, ADC_SAMPLES, sizeof(adc_value_t), adc_comp);
  
#if ADC_SAMPLES > 1 /* have average filter */
  uint32_t accum = 0;
  
  value = values + (ADC_SAMPLES - ADC_SELECT) / 2;
  end = value + ADC_SELECT;
  
  for(; value < end; ){
    accum += *value++;
  }
  
  return accum / ADC_SELECT;
#else /* haven't average filter */
  return values[ADC_SAMPLES / 2 + 1];
#endif /* have average filter */

#else /* haven't median filter */
#if ADC_SAMPLES > 1 /* have average filter */
  uint64_t accum = 0;
  int i;
  
  for(i = 0; i < ADC_SAMPLES; i++){
    accum += *field;
    field += ADC_CHNNELS;
  }

  return accum / ADC_SAMPLES;
#else /* haven't average filter */
  return *field;
#endif /* have average filter */
#endif /* have median filter */
}

static adc_values_t adc_values;

static inline void sensor_convert(void) {
  adc_values.Tmcu = adc_value_sample(&adc_buffer->Tmcu);
  adc_values.Uref = adc_value_sample(&adc_buffer->Uref);
#ifdef TEMP1
  adc_values.Ths1 = adc_value_sample(&adc_buffer->Ths1);
#endif
#ifdef TEMP2
  adc_values.Ths2 = adc_value_sample(&adc_buffer->Ths2);
#endif
  adc_values.Isen = adc_value_sample(&adc_buffer->Isen);
  adc_values.Usen = adc_value_sample(&adc_buffer->Usen);
}

static adc_value_t adc_value(const adc_value_t *field) {
  adc_value_t value = *((const adc_value_t*)&adc_values + (field - (const adc_value_t*)&adc_buffer));
  return value;
}

#ifdef VREF
static inline fix16 sensor_Uref(void) {
  return fix16_mul(fix16_make(adc_value(&adc_buffer->Uref)),
                   fix16_make((float)ADC_VTOP / (float)ADC_TOP));
}
#endif

#ifdef TMCU
/* Tmcu (deg. C) = (U_sense_T0 - U_sense) / Avg_Slope + T[0], T0 = 25 (or 30) */
/* Tmcu (deg. C) = (U_sense_T0 / Avg_Slope + T0) - U_sense / Avg_Slope */

#define TMCU_CONV(type, src)                                          \
  math_op(type, sub,                                                  \
          math_op(type, make, (float)TMCU_VSENSE_T0 /                 \
                  (float)TMCU_AVG_SLOPE + (float)TMCU_TEMPER_T0),     \
          math_op(type, mul,                                          \
                  math_op(type, make, src),                           \
                  math_op(type, make, (float)ADC_VTOP /               \
                          (float)ADC_OVER / (float)TMCU_AVG_SLOPE)))

static inline fix16 sensor_Tmcu(void) {
  return TMCU_CONV(fix16, adc_value(&adc_buffer->Tmcu));
}
#endif

#define RANGE_CONV(type, val, range)                       \
  scale_val_const(type, math_op(type, make, val),          \
                  _CAT2(range, _OFF) * ADC_UV,             \
                  _CAT2(range, _LIM) * ADC_UV,             \
                  _CAT2(range, _MIN), _CAT2(range, _MAX))

curr_filter_param_t curr_filter_param;
#define curr_filter_state_s _CAT2(CURR_FILTER, _state_t)(fix16)
static curr_filter_state_s curr_filter_state;

static inline fix16 sensor_Isen(void) {
  fix16 src = RANGE_CONV(fix16, adc_value(&adc_buffer->Isen), I_SEN);
  
  return filter_inited(CURR_FLAG) ?
    _CAT2(CURR_FILTER, _step)(fix16, &curr_filter_param, &curr_filter_state, src) :
    _CAT2(CURR_FILTER, _init)(fix16, &curr_filter_state, src);
}

volt_filter_param_t volt_filter_param;
#define volt_filter_state_s _CAT2(VOLT_FILTER, _state_t)(fix16)
static volt_filter_state_s volt_filter_state;

static inline fix16 sensor_Usen(void) {
  fix16 src = RANGE_CONV(fix16, adc_value(&adc_buffer->Usen), U_SEN);

  return filter_inited(VOLT_FLAG) ?
    _CAT2(VOLT_FILTER, _step)(fix16, &volt_filter_param, &volt_filter_state, src) :
    _CAT2(VOLT_FILTER, _init)(fix16, &volt_filter_state, src);
}

/**
 * @brief Convert ADC value to resistance.
 *
 *        Analog VCC
 *
 *            ^
 *            |
 *        .---o---.
 *        |       |
 *       .-.     .-.
 *       | |     | |
 *    R2 | |  Rt | |
 *       | |     | |
 *       '-'     '-'
 *        |       |
 *        '---o---o----> Analog IN
 *            |
 *           .-.
 *           | |
 *        R1 | |
 *           | |
 *           '-'
 *           _|_
 *
 *        Analog GND
 *
 * If r2 doesn't used, r2 must be set to 0
 */
#define A2R_HI_WITH_R2(A, R1, R2) (((double)(ADC_TOP) - (A)) * (R1) * (R2) / (((double)(A) - (ADC_TOP)) * (R1) + (R2)))
#define A2R_HI_WITHOUT_R2(A, R1) (((double)(ADC_TOP) - (A)) * (R1) / (double)(A))
#define A2R_HI_HELP(A, R1, R2) ((R2) == 0 ? A2R_HI_WITHOUT_R2(A, R1) : A2R_HI_WITH_R2(A, R1, R2))
#define A2R_HI(A, RS) A2R_HI_HELP(A, _NTH0(RS), _NTH1(RS))

#define R2A_HI_WITH_R2(R, R1, R2) ((((double)(ADC_TOP) * (R1) - (R)) * (R2) + (double)(ADC_TOP) * (R) * (R1)) / ((R1) * ((R2) + (R))))
#define R2A_HI_WITHOUT_R2(R, R1) ((double)(ADC_TOP) * (R1) / ((R1) + (R)))
#define R2A_HI_HELP(R, R1, R2) ((R2) == 0 ? R2A_HI_WITHOUT_R2(R, R1) : R2A_HI_WITH_R2(R, R1, R2))
#define R2A_HI(R, RS) R2A_HI_HELP(R, _NTH0(RS), _NTH1(RS))

/**
 * @brief Steinhart-Hart NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define SH_R2K_HELP(l, a, b, c) (1.0 / ((a) + (b) * (l) + (c) * pow((l), 3)))
#define SH_R2K(R, P...) SH_R2K_HELP(log(R), ##P)

/**
 * @brief Simplified beta NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define BETA_R2K_HELP(R, BETA, R0, T0) (1.0 / (1.0 / (T0) + 1.0 / (BETA) * log((R) / (R0))))
#define BETA_R2K(R, P) BETA_R2K_HELP(R, _NTH0(P), _NTH1(P), _NTH2(P))

#define BETA_K2R_HELP(K, BETA, R0, T0) (R0 * exp(BETA / K - BETA / T0))
#define BETA_K2R(K, P) BETA_K2R_HELP(R, _NTH0(P), _NTH1(P), _NTH2(P))

/**
 * @brief Kelvins to Celsius
 */
#define K2C(K) ((K)-273.15)

/**
 * @brief Celsius to Kelvins
 */
#define C2K(C) ((C)+273.15)

#define therm2adc(t) R2A_HI(_CAT2(THERM_MODEL, _K2R)(C2K((double)(t)), THERM_PARAMS), THERM_RESISTORS)
#define adc2therm(a) K2C(_CAT2(THERM_MODEL, _R2K)(A2R_HI(a, THERM_RESISTORS), THERM_PARAMS))

#define THERM_RES_FROM R2A_HI(TEMP_LOOKUP_FROM, THERM_RESISTORS)
#define THERM_RES_TO R2A_HI(TEMP_LOOKUP_TO, THERM_RESISTORS)
#define THERM_RES_STEP ((THERM_RES_TO - THERM_RES_FROM) / TEMP_LOOKUP_SIZE)

#include lookup_table_file(ntc100k)
#define ntc100k_fn(s) fix16_make(adc2therm(THERM_RES_FROM + (s) * THERM_RES_STEP))
lookup_table(fix16, ntc100k, ntc100k_fn,
             fix16_make(THERM_RES_FROM),
             fix16_make(THERM_RES_STEP));

temp_filter_param_t temp_filter_param;
#define temp_filter_state_s _CAT2(TEMP_FILTER, _state_t)(fix16)
static temp_filter_state_s temp_filter_state[2];

static inline fix16 sensor_Ths(uint8_t n){
  uint16_t adc = 0;
  switch (n) {
#ifdef TEMP1
  case 0:
    adc = adc_value(&adc_buffer->Ths1);
    break;
#endif
#ifdef TEMP2
  case 1:
    adc = adc_value(&adc_buffer->Ths2);
    break;
#endif
  }
  
  fix16 src = ntc100k(fix16_make(adc));
  
  return filter_inited(n) ?
    _CAT2(TEMP_FILTER, _step)(fix16, &temp_filter_param,
                              &temp_filter_state[n], src) :
    _CAT2(TEMP_FILTER, _init)(fix16, &temp_filter_state[n], src);
}

void sensor_init(void) {
  /* Enable ADC_DEV clock */
  rcc_periph_clock_enable(ADC_RCC);
  
  /* Enable ADC_DMA channel interrupt */
  nvic_enable_irq(ADC_DMA_IRQ);

  /* Configure sensor GPIO */
#if VOLT_PORT == CURR_PORT
	gpio_set_mode(VOLT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                VOLT_PAD | CURR_PAD);
#else
  gpio_set_mode(VOLT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                VOLT_PAD);
  gpio_set_mode(CURR_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                CURR_PAD);
#endif
  
#ifdef TEMP1
  gpio_set_mode(TEMP1_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                TEMP1_PAD);
#endif

#ifdef TEMP2
  gpio_set_mode(TEMP2_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                TEMP2_PAD);
#endif

#ifdef TEST
  gpio_set_mode(TEST_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                TEST_PAD);
#endif

  /* Configure ADC subsystem */
  adc_power_off(ADC_DEV);
  
  /* ADC frequency (ADCCLK) is 24MHz/2 = 12MHz and period is 1/12 uS */
  /* ADC frequency (ADCCLK) is 24MHz/4 = 6MHz and period is 1/6 uS */
  /* ADC frequency (ADCCLK) is 24MHz/8 = 3MHz and period is 1/3 uS */
  rcc_set_adcpre(ADC_PRE);
  
  adc_power_on(ADC_DEV);
  
  adc_reset_calibration(ADC_DEV);
  adc_calibration(ADC_DEV);

#ifdef ADC_TRIG
  adc_enable_external_trigger_regular(ADC_DEV, ADC_TRIG);
#endif
  adc_set_single_conversion_mode(ADC_DEV);
  adc_set_right_aligned(ADC_DEV);

#ifdef TMCU
  /* ADC conversion time for Tmcu (Tconv) is 239.5 + 12.5 = 252 cycles */
  /* 252 cycles / 12 MHz = 21 uS > 17.1 uS is ok */
  adc_set_sample_time(ADC_DEV, ADC_CHANNEL_TEMP, ADC_SMPR_SMP_239DOT5CYC); /* Tmcu */
#endif

#ifdef VREF
  /* ADC conversion time for Vref (Tconv) is 28.5 + 12.5 = 41 cycles */
  /* 41 cycles / 12 MHz = 3.4 uS */
  adc_set_sample_time(ADC_DEV, ADC_CHANNEL_VREF, ADC_SMPR_SMP_28DOT5CYC); /* Ur */
#endif

  /* ADC conversion time for Tmcu (Tconv) is 71.5 + 12.5 = 84 cycles */
  /* 84 cycles / 12 MHz = 7 uS */
  adc_set_sample_time(ADC_DEV, CURR_CH, ADC_SMPR_SMP_71DOT5CYC); /* Is */
  adc_set_sample_time(ADC_DEV, VOLT_CH, ADC_SMPR_SMP_71DOT5CYC); /* Us */
  
  /* ADC conversion time for Tmcu (Tconv) is 71.5 + 12.5 = 84 cycles */
  /* 84 cycles / 12 MHz = 7 uS */
#ifdef TEMP1
  adc_set_sample_time(ADC_DEV, TEMP1_CH, ADC_SMPR_SMP_55DOT5CYC); /* T1 */
#endif
#ifdef TEMP2
  adc_set_sample_time(ADC_DEV, TEMP2_CH, ADC_SMPR_SMP_55DOT5CYC); /* T2 */
#endif

#ifdef TMCU
  adc_enable_temperature_sensor(ADC_DEV);
#endif

#ifdef VREF
  //adc_enable_vrefint(ADC_DEV);
#endif
  
  adc_enable_scan_mode(ADC_DEV);
  
  {
    uint8_t channel[] = {
#ifdef TMCU
      ADC_CHANNEL_TEMP,
#endif
#ifdef VREF
      ADC_CHANNEL_VREF,
#endif
      CURR_CH,
      VOLT_CH,
#ifdef TEMP1
      TEMP1_CH,
#endif
#ifdef TEMP2
      TEMP2_CH,
#endif
    };
    adc_set_regular_sequence(ADC_DEV, sizeof(channel)/sizeof(channel[0]), channel);
  }
  
  adc_enable_dma(ADC_DEV);
  
  dma_channel_reset(ADC_DMA, ADC_DMA_CH);
  dma_set_priority(ADC_DMA, ADC_DMA_CH, DMA_CCR_PL_VERY_HIGH);
  dma_set_peripheral_size(ADC_DMA, ADC_DMA_CH, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(ADC_DMA, ADC_DMA_CH, DMA_CCR_MSIZE_16BIT);
  dma_enable_memory_increment_mode(ADC_DMA, ADC_DMA_CH);
  dma_enable_circular_mode(ADC_DMA, ADC_DMA_CH);
  dma_set_read_from_peripheral(ADC_DMA, ADC_DMA_CH);
  dma_set_peripheral_address(ADC_DMA, ADC_DMA_CH, (uint32_t)&ADC_DR(ADC_DEV));
  dma_set_memory_address(ADC_DMA, ADC_DMA_CH, (uint32_t)&adc_buffer);
  dma_set_number_of_data(ADC_DMA, ADC_DMA_CH, sizeof(adc_buffer)/sizeof(adc_value_t));
  
  /* Enable transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(ADC_DMA, ADC_DMA_CH);
  
  /* Start DMA transfer */
  dma_enable_channel(ADC_DMA, ADC_DMA_CH);

#ifdef TRIG_DEV
  /* Configure trigger timer */

  /* Enable TIM clock */
  rcc_periph_clock_enable(TRIG_RCC);
  
  /* Reset TIM peripheral */
  timer_reset(TRIG_DEV);

  /* Timer global mode:
   * - No divider
   * - Alignment edge
   * - Direction up
   */
  timer_set_mode(TRIG_DEV,
                 TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE,
                 TIM_CR1_DIR_UP);

  /* Reset prescaler value */
  timer_set_prescaler(TRIG_DEV, TRIG_PSC);

  /* Enable preload */
  timer_enable_preload(TRIG_DEV);

  /* Continuous mode */
  timer_continuous_mode(TRIG_DEV);

  /* Period */
  timer_set_period(TRIG_DEV, TRIG_TOP);

  /* ARR reload enable */
  timer_enable_preload(TRIG_DEV);

  /* Disable output */
  timer_disable_oc_output(TRIG_DEV, TRIG_OC);
  
  /* Configure line */
  timer_enable_oc_preload(TRIG_DEV, TRIG_OC);
  timer_set_oc_mode(TRIG_DEV, TRIG_OC, TIM_OCM_PWM1);
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(TRIG_DEV, TRIG_OC, 1);
  
  /* Enable output */
  timer_enable_oc_output(TRIG_DEV, TRIG_OC);

  /* Enable master mode */
  timer_set_master_mode(TRIG_DEV, TRIG_MMS);
  
  /* Enable counter */
  timer_enable_counter(TRIG_DEV);
#endif /* TRIG_DEV */

#ifndef ADC_TRIG
  adc_start_conversion_direct(ADC_DEV);
#endif
}

void sensor_done(void) {
#ifdef TRIG_DEV
  /* Deconfigure trigger timer */
  
  /* Disable counter */
  timer_disable_counter(TRIG_DEV);
  
  /* Disable output */
  timer_disable_oc_output(TRIG_DEV, TRIG_OC);
  
  /* Disable clock */
  rcc_periph_clock_disable(TRIG_RCC);
#endif /* TRIG_DEV */
  
  /* Disable the ADC_DEV */
  adc_power_off(ADC_DEV);

#ifdef TEST
  gpio_set_mode(TEST_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                TEST_PAD);
#endif
  
#if VOLT_PORT == CURR_PORT
	gpio_set_mode(VOLT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                VOLT_PAD | CURR_PAD);
#else
  gpio_set_mode(VOLT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                VOLT_PAD);
  gpio_set_mode(CURR_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                CURR_PAD);
#endif
  
#ifdef TEMP1
  gpio_set_mode(TEMP1_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                TEMP1_PAD);
#endif

#ifdef TEMP2
  gpio_set_mode(TEMP2_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                TEMP2_PAD);
#endif
  
  /* Disable the ADC_DMA channel */
  dma_disable_transfer_complete_interrupt(ADC_DMA, ADC_DMA_CH);
  dma_disable_channel(ADC_DMA, ADC_DMA_CH);

  /* Disable ADC_DMA channel interrupt */
  nvic_disable_irq(ADC_DMA_IRQ);
  
  /* Disable ADC_DEV clock */
  rcc_periph_clock_disable(ADC_RCC);
}

/* Initial adc conversion complete */
void adc_dma_isr(void) {
  dma_clear_interrupt_flags(ADC_DMA, ADC_DMA_CH, DMA_GIF | DMA_TCIF);

#ifdef TEST
  gpio_set(TEST_PORT, TEST_PAD);
#endif

  sensor_convert();

  Uref = sensor_Uref();
  Tmcu = sensor_Tmcu();

  Isen = sensor_Isen();
  Usen = sensor_Usen();

#ifdef TEMP1
  Ths1 = sensor_Ths(0);
#endif

#ifdef TEMP2
  Ths2 = sensor_Ths(1);
#endif

  sensor_step();

#ifdef TEST
  gpio_clear(TEST_PORT, TEST_PAD);
#endif
}
