#include "panel.h"

#include "l10n.h"

#if USE_PARAMS
#include "params.h"
#include "param-sio.h"
#include "param-intern.h"
#endif

#include "pcd8544.h"
#include "bitmap.h"

#include "driver.h"
#include "sensor.h"

#include "macro.h"
#include "config.h"

#include "btn.h"
#include "enc.h"

btn_def();
enc_def();

static bitmap_state_t canvas_state;

static const bitmap_canvas_t canvas = {
  pcd8544_frame_buffer,
  { {{ _NTH0(bitmap_config), _NTH1(bitmap_config) }},
    {{ _NTH2(bitmap_config), _NTH3(bitmap_config) }} },
  &canvas_state
};

static bitmap_coord_t glyph_w;
static bitmap_coord_t glyph_h;
static bitmap_coord_t text_rows;
static bitmap_coord_t text_cols;

static void
bitmap_param(const bitmap_canvas_t *bitmap,
             bitmap_coord_t c,
             bitmap_coord_t r,
             bitmap_coord_t l,
             const param_desc_t *param) {
  const char *name = NULL; //param_text(param, param_item);
  uint8_t name_len = name ? strlen(name) : 0;
  
  const char *unit = param_text(param, param_unit);
  uint8_t unit_len = unit ? strlen(unit) : 0;
  
  uint8_t data_len = l - ((name ? bitmap_strlen(name) : 0) + 1 + (unit ? bitmap_strlen(unit) : 0));
  char line[l*2 + 1];
  
  memcpy(line, name, name_len);
  line[name_len] = ' ';
  
  char *ptr = line + name_len + 1;
  char *ptr_ = ptr;
  if ((param_success == param_getstr(param, &ptr, ptr + data_len)) || 1) {
    memset(ptr, ' ', ptr_ + data_len - ptr);
  } else {
    memset(ptr_, '*', data_len);
  }
  
  memcpy(line + name_len + 1 + data_len, unit, unit_len);
  line[name_len + 1 + data_len + unit_len] = '\0';
  
  bitmap_text(bitmap, c * glyph_w, r * glyph_h, 0, 0, line);
}

typedef enum {
  adj_idle,
  adj_navi,
  adj_tune,
} adj_state_t;

static adj_state_t adj_state = adj_idle;
static const param_desc_t *adj_param = NULL;

static void sel_param(int8_t off) {
  param_num_t num = adj_param ? param_num(&params, adj_param) + off : off;
  param_num_t top = param_num(&params, NULL);
  
  if (off == 0) {
    off = 1;
  }
  
  for (; ; num += off) {
    if (num >= top && off == 1) {
      off = -1;
      continue;
    }
    
    adj_param = param_desc(&params, num);
    
    if (param_text(adj_param, param_item)) {
      break;
    }

    if (num == 0 && off == -1) {
      off = 1;
    }
  }
}

static void
display_draw(void) {
  for (; pcd8544_active(); );
  
  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);
  
  bitmap_operation(&canvas, bitmap_draw);

  switch (adj_state) {
  case adj_idle:
    bitmap_text(&canvas, 0, 0, 0, 0, l10n("Main", "Главный"));
    bitmap_param(&canvas, 0, 1, text_cols, params_ptr(Ths1));
    bitmap_param(&canvas, 0, 2, text_cols, params_ptr(Ths2));
    bitmap_param(&canvas, 0, 3, text_cols, params_ptr(Usen));
    bitmap_param(&canvas, 0, 4, text_cols, params_ptr(Isen));
    break;
  case adj_navi:
    bitmap_text(&canvas, 0, 0, 0, 0, l10n("Select", "Выбор"));
    bitmap_text(&canvas, 0, glyph_h, 0, 0, param_text(adj_param, param_item));
    bitmap_param(&canvas, 0, 2, text_cols, adj_param);
    break;
  case adj_tune:
    bitmap_text(&canvas, 0, 0, 0, 0, l10n("Adjust", "Установка"));
    bitmap_text(&canvas, 0, glyph_h, 0, 0, param_text(adj_param, param_item));
    bitmap_param(&canvas, 0, 2, text_cols, adj_param);
    break;
  }
  
  pcd8544_update();
}

static void
display_on(void) {
  pcd8544_init();
  
  bitmap_init(&canvas);
  
  bitmap_primitive(&canvas, bitmap_line);
  bitmap_align(&canvas, bitmap_top | bitmap_left);
  bitmap_font(&canvas, font_8x8);
  
  {
    bitmap_glyph_size(&canvas, &glyph_w, &glyph_h);
    text_cols = canvas.state->cols / glyph_w;
    text_rows = canvas.state->rows / glyph_h;
  }
  
  display_draw();
  
  pcd8544_backlight(100);
}

static void
display_off(void) {
  pcd8544_backlight(0);
  
  pcd8544_done();
}

void panel_init(void) {
  pcd8544_init();
  pcd8544_done();
  
  btn_init();
  enc_init();
}

void panel_done(void) {
  btn_done();
  enc_done();
}

void panel_step(void) {
  evt_t e;

  switch (ctl_state) {
  case state_off:
    evt_each(e) {
      switch (e) {
      case evt_enter:
        ctl_state = state_on;
        display_on();
        break;
      }
    }
    break;
  case state_on:
    evt_each(e) {
      switch (adj_state) {
      case adj_idle:
        switch (e) {
        case evt_enter:
          ctl_state = state_off;
          display_off();
          break;
        case evt_setup:
          adj_state = adj_navi;
          sel_param(0);
          display_draw();
          break;
        case evt_tick:
          display_draw();
          break;
        }
        break;
      case adj_navi:
        switch (e) {
        case evt_enter:
          if (param_flag(adj_param) & param_setable) {
            adj_state = adj_tune;
          }
          break;
        case evt_setup:
          adj_state = adj_idle;
          break;
        case evt_incr:
          sel_param(1);
          break;
        case evt_decr:
          sel_param(-1);
          break;
        }
        display_draw();
        break;
      case adj_tune:
        switch (e) {
        case evt_enter:
          param_save(adj_param);
          adj_state = adj_navi;
          break;
        case evt_setup:
          param_load(adj_param);
          adj_state = adj_navi;
          break;
        case evt_incr:
          param_raw_adj(adj_param, 1);
          break;
        case evt_decr:
          param_raw_adj(adj_param, -1);
          break;
        }
        display_draw();
        break;
      }
    }
    break;
  }
}
