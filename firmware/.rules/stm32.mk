# presets for compiler, linker and debugger (rules.mk)
stm32.CMACH ?= thumb
stm32.SPECS ?= nosys nano
stm32.DUMPOPTS ?= -b elf32-littlearm -m arm -M force-thumb
stm32.GDBOPTS ?= -ex 'target remote $(GDBREMOTE)'

stm32f0.INHERIT := stm32
stm32f0.CDEFS ?= STM32F0
stm32f0.CMACH ?= cpu=cortex-m0 soft-float

stm32f1.INHERIT := stm32
stm32f1.CDEFS ?= STM32F1
stm32f1.CMACH ?= cpu=cortex-m3 soft-float fix-cortex-m3-ldrd

stm32f2.INHERIT := stm32
stm32f2.CDEFS ?= STM32F2
stm32f2.CMACH ?= cpu=cortex-m3 soft-float fix-cortex-m3-ldrd

stm32f3.INHERIT := stm32
stm32f3.CDEFS ?= STM32F3
stm32f3.CMACH ?= cpu=cortex-m4 float-abi=hard fpu=fpv4-sp-d16

stm32f4.INHERIT := stm32
stm32f4.CDEFS ?= STM32F4
stm32f4.CMACH ?= cpu=cortex-m4 float-abi=hard fpu=fpv4-sp-d16

# presets for ls scripts generator (ldgen.mk)
stm32.ROM.ADDR := 0x08000000
stm32.RAM.ADDR := 0x20000000

stm32f0.INCLUDE := libopencm3_stm32f0.ld

$(call LDS_PRESET,stm32f0,30x4,16K,4K)
$(call LDS_PRESET,stm32f0,30x6,32K,4K)
$(call LDS_PRESET,stm32f0,30x8,64K,8K)
$(call LDS_PRESET,stm32f0,30xc,256K,32K)

stm32f1.INCLUDE := libopencm3_stm32f1.ld

$(call LDS_PRESET,stm32f1,00x4,16K,4K)
$(call LDS_PRESET,stm32f1,00x6,32K,4K)
$(call LDS_PRESET,stm32f1,00x8,64K,8K)
$(call LDS_PRESET,stm32f1,00xb,128K,8K)

$(call LDS_PRESET,stm32f1,03x4,16K,6K)
$(call LDS_PRESET,stm32f1,03x6,32K,10K)
$(call LDS_PRESET,stm32f1,03x8,64K,20K)
$(call LDS_PRESET,stm32f1,03xb,128K,20K)

uartboot.BOOTLD.SIZE := 2K
usbdfuboot.BOOTLD.SIZE := 4K

params1k.PARAMS.SIZE := 1K
params2k.PARAMS.SIZE := 2K
params4k.PARAMS.SIZE := 4K
params8k.PARAMS.SIZE := 8K
