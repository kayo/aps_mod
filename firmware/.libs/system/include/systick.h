#ifndef SYSTICK_H
#define SYSTICK_H "systick.h"

void systick_init(void);
void systick_done(void);

typedef uint32_t systick_t;
systick_t systick_read(void);

#endif /* SYSTICK_H */
