#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "pwm.h"

#include "macro.h"
#include "config.h"

#define PWM_NUM instance

pwm_def(PWM_NUM);

#define PWM_CFG _CAT3(pwm, PWM_NUM, _config)

#define _CAT1(x) x

#define PWM_PRESCALER _NTH1(PWM_CFG)
#define PWM_PERIOD _NTH2(PWM_CFG)
#define PWM_OUTPUT _UNWR(_NTH3(PWM_CFG))

#define TIM_NUM _NTH0(PWM_CFG)
#define TIM_DEV _CAT2(TIM, TIM_NUM)
#define TIM_RCC _CAT2(RCC_TIM, TIM_NUM)
#define TIM_REMAP _NTH4(PWM_CFG)

#define PWM_OUT_nc 0
#define PWM_OUT_pp 1
#define PWM_OUT_od 2

#ifdef STM32F1
#define GPIO_CFG_pp PUSHPULL
#define GPIO_CFG_od OPENDRAIN
#else
#define GPIO_CFG_pp PP
#define GPIO_CFG_od OD
#endif

#if _CAT2(PWM_OUT_, _NTH0(PWM_OUTPUT))
#define CH1_CFG _CAT2(GPIO_CFG_, _NTH0(PWM_OUTPUT))
#define CH1_PORT _CAT5(GPIO_BANK_TIM, TIM_NUM, TIM_REMAP, _CH, 1)
#define CH1_PAD _CAT5(GPIO_TIM, TIM_NUM, TIM_REMAP, _CH, 1)
#define CH1_AF _CAT2(GPIO, TIM_REMAP)
#endif

#if _CAT2(PWM_OUT_, _NTH1(PWM_OUTPUT))
#define CH2_CFG _CAT2(GPIO_CFG_, _NTH1(PWM_OUTPUT))
#define CH2_PORT _CAT5(GPIO_BANK_TIM, TIM_NUM, TIM_REMAP, _CH, 2)
#define CH2_PAD _CAT5(GPIO_TIM, TIM_NUM, TIM_REMAP, _CH, 2)
#define CH2_AF _CAT2(GPIO, TIM_REMAP)
#endif

#if _CAT2(PWM_OUT_, _NTH2(PWM_OUTPUT))
#define CH3_CFG _CAT2(GPIO_CFG_, _NTH2(PWM_OUTPUT))
#define CH3_PORT _CAT5(GPIO_BANK_TIM, TIM_NUM, TIM_REMAP, _CH, 3)
#define CH3_PAD _CAT5(GPIO_TIM, TIM_NUM, TIM_REMAP, _CH, 3)
#define CH3_AF _CAT2(GPIO, TIM_REMAP)
#endif

#if _CAT2(PWM_OUT_, _NTH3(PWM_OUTPUT))
#define CH4_CFG _CAT2(GPIO_CFG_, _NTH3(PWM_OUTPUT))
#define CH4_PORT _CAT5(GPIO_BANK_TIM, TIM_NUM, TIM_REMAP, _CH, 4)
#define CH4_PAD _CAT5(GPIO_TIM, TIM_NUM, TIM_REMAP, _CH, 4)
#define CH4_AF _CAT2(GPIO, TIM_REMAP)
#endif

static void
pwm_config_output(enum tim_oc_id oc) {
  /* Disable output */
  timer_disable_oc_output(TIM_DEV, oc);
  
  /* Configure global mode of line */
  timer_disable_oc_clear(TIM_DEV, oc);
  timer_enable_oc_preload(TIM_DEV, oc);
  timer_set_oc_slow_mode(TIM_DEV, oc);
  timer_set_oc_mode(TIM_DEV, oc, TIM_OCM_PWM1);
  
  /* Configure OC */
  timer_set_oc_polarity_high(TIM_DEV, oc);
  timer_set_oc_idle_state_unset(TIM_DEV, oc);
  
  /* Set the output compare value for OC */
  timer_set_oc_value(TIM_DEV, oc, 0);
  
  /* Enable output */
  timer_enable_oc_output(TIM_DEV, oc);
}

void pwm_fn(PWM_NUM, init)(void) {
#ifdef STM32F1
#ifdef CH1_CFG
  gpio_set_mode(CH1_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                _CAT2(GPIO_CNF_OUTPUT_ALTFN_, CH1_CFG), CH1_PAD);
#endif
#ifdef CH2_CFG
  gpio_set_mode(CH2_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                _CAT2(GPIO_CNF_OUTPUT_ALTFN_, CH2_CFG), CH2_PAD);
#endif
#ifdef CH3_CFG
  gpio_set_mode(CH3_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                _CAT2(GPIO_CNF_OUTPUT_ALTFN_, CH3_CFG), CH3_PAD);
#endif
#ifdef CH4_CFG
  gpio_set_mode(CH4_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                _CAT2(GPIO_CNF_OUTPUT_ALTFN_, CH4_CFG), CH4_PAD);
#endif
#else /* !STM32F1 */
#ifdef CH1_CFG
  gpio_mode_setup(CH1_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CH1_PAD);
  gpio_set_output_options(CH1_PORT, _CAT2(GPIO_OTYPE_, CH1_CFG),
						  GPIO_OSPEED_HIGH, CH1_PAD);
  gpio_set_af(CH1_PORT, CH1_AF, CH1_PAD);
#endif
#ifdef CH2_CFG
  gpio_mode_setup(CH2_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CH2_PAD);
  gpio_set_output_options(CH2_PORT, _CAT2(GPIO_OTYPE_, CH2_CFG),
						  GPIO_OSPEED_HIGH, CH2_PAD);
  gpio_set_af(CH2_PORT, CH2_AF, CH2_PAD);
#endif
#ifdef CH3_CFG
  gpio_mode_setup(CH3_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CH3_PAD);
  gpio_set_output_options(CH3_PORT, _CAT2(GPIO_OTYPE_, CH3_CFG),
						  GPIO_OSPEED_HIGH, CH3_PAD);
  gpio_set_af(CH3_PORT, CH3_AF, CH3_PAD);
#endif
#ifdef CH4_CFG
  gpio_mode_setup(CH4_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CH4_PAD);
  gpio_set_output_options(CH4_PORT, _CAT2(GPIO_OTYPE_, CH4_CFG),
						  GPIO_OSPEED_HIGH, CH4_PAD);
  gpio_set_af(CH4_PORT, CH4_AF, CH4_PAD);
#endif
#endif /* !STM32F1 */
  
  /* Enable TIM_DEV for backlight control */
  rcc_periph_clock_enable(TIM_RCC);
  
  timer_reset(TIM_DEV);
  
  /* Timer global mode:
   * - No divider
   * - Alignment edge
   * - Direction up
   */
  timer_set_mode(TIM_DEV,
                 TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE,
                 TIM_CR1_DIR_UP);
  
  timer_set_prescaler(TIM_DEV, PWM_PRESCALER);
  timer_set_repetition_counter(TIM_DEV, 0);
  timer_enable_preload(TIM_DEV);
  timer_continuous_mode(TIM_DEV);
  
#if 0
  /* Configure break and deadtime */
  timer_set_deadtime(TIM_DEV, 0);
  timer_set_enabled_off_state_in_idle_mode(TIM_DEV);
  timer_set_enabled_off_state_in_run_mode(TIM_DEV);
  timer_disable_break(TIM_DEV);
  timer_set_break_polarity_high(TIM_DEV);
  timer_disable_break_automatic_output(TIM_DEV);
  timer_set_break_lock(TIM_DEV, TIM_BDTR_LOCK_OFF);
#endif
  
#ifdef CH1_CFG
  pwm_config_output(TIM_OC1);
#endif
#ifdef CH2_CFG
  pwm_config_output(TIM_OC2);
#endif
#ifdef CH3_CFG
  pwm_config_output(TIM_OC3);
#endif
#ifdef CH4_CFG
  pwm_config_output(TIM_OC4);
#endif

  timer_set_period(TIM_DEV, PWM_PERIOD);
  timer_enable_break_main_output(TIM_DEV);
  timer_enable_counter(TIM_DEV);
}

void pwm_fn(PWM_NUM, done)(void) {
  rcc_periph_clock_disable(TIM_RCC);

#ifdef STM32F1
#ifdef CH1_CFG
  gpio_set_mode(CH1_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CH1_PAD);
#endif
#ifdef CH2_CFG
  gpio_set_mode(CH2_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CH2_PAD);
#endif
#ifdef CH3_CFG
  gpio_set_mode(CH3_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CH3_PAD);
#endif
#ifdef CH4_CFG
  gpio_set_mode(CH4_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CH4_PAD);
#endif
#else
  
#endif
}

#ifdef CH1_CFG
void pwm_fn(PWM_NUM, set1)(uint16_t val) {
  timer_set_oc_value(TIM_DEV, TIM_OC1, val);
}
#endif

#ifdef CH2_CFG
void pwm_fn(PWM_NUM, set2)(uint16_t val) {
  timer_set_oc_value(TIM_DEV, TIM_OC2, val);
}
#endif

#ifdef CH3_CFG
void pwm_fn(PWM_NUM, set3)(uint16_t val) {
  timer_set_oc_value(TIM_DEV, TIM_OC3, val);
}
#endif

#ifdef CH4_CFG
void pwm_fn(PWM_NUM, set4)(uint16_t val) {
  timer_set_oc_value(TIM_DEV, TIM_OC4, val);
}
#endif
