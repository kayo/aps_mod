#include "ticks.h"

#define TICKS_MAX UINT32_MAX

ticks_t ticks_diff(ticks_t from, ticks_t to) {
  return from <= to ? to - from : TICKS_MAX - from + to;
}
