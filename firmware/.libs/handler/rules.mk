libhandler.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libhandler
libhandler.INHERIT := firmware libopencm3 libsystem
libhandler.CDIRS := $(libhandler.BASEPATH)include
libhandler.CDEFS = $(if $(libhandler.events),EVT_BUFFER=$(libhandler.events))
libhandler.modules ?=
libhandler.modules += evt
libhandler.SRCS := $(foreach m,$(libhandler.modules),\
$(libhandler.BASEPATH)src/$(word 1,$(subst :, ,$(m))).c$(if $(word 2,$(subst :, ,$(m))),?instance=$(word 2,$(subst :, ,$(m)))))
