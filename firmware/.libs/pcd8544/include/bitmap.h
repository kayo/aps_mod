#ifndef BITMAP_H
#define BITMAP_H "bitmap.h"

#ifndef BITMAP_USE_STDFIX
#define BITMAP_USE_STDFIX 0
#endif

#ifndef BITMAP_USE_AFFINE
#define BITMAP_USE_AFFINE 0
#endif

#ifndef BITMAP_USE_CHECK
#define BITMAP_USE_CHECK 0
#endif

#ifndef BITMAP_COORD_BITS
#define BITMAP_COORD_BITS 8
#endif

#include <stddef.h>

#if BITMAP_USE_STDFIX

#include <stdfix.h>

#else /* !BITMAP_USE_STDFIX */

#include <stdint.h>

#if BITMAP_COORD_BITS == 8
/**
 * @brief Bitmap coord type
 */
typedef int8_t bitmap_coord_t;
#elif BITMAP_COORD_BITS == 16
typedef int16_t bitmap_coord_t;
#elif BITMAP_COORD_BITS == 32
typedef int32_t bitmap_coord_t;
#endif

#endif /* BITMAP_USE_STDFIX */

typedef uint8_t bitmap_cell_t;
typedef size_t bitmap_size_t;

/**
 * @brief Bitmap point type
 */
typedef union {
  struct {
    bitmap_coord_t x;
    bitmap_coord_t y;
  };
  bitmap_coord_t p[2];
} bitmap_point_t;

/**
 * @brief Bitmap bounding box type
 */
typedef struct {
  bitmap_point_t a;
  bitmap_point_t b;
} bitmap_bound_t;

#if BITMAP_USE_AFFINE
/**
 * @brief Bitmap matrix type
 *
 * Affine transformation matrix.
 */
typedef union {
  struct {
    /* column 0 */
    bitmap_coord_t a;
    bitmap_coord_t b;
    /* 0 */

    /* column 1 */
    bitmap_coord_t c;
    bitmap_coord_t d;
    /* 0 */

    /* column 2 */
    bitmap_coord_t e;
    bitmap_coord_t f;
    /* 1 */
  };
  bitmap_coord_t p[6];
} bitmap_matrix_t;
#endif

/**
 * @brief Bitmap drawing state
 */
typedef enum {
  bitmap_left = 1 << 0,
  bitmap_right = 1 << 1,
  bitmap_top = 1 << 2,
  bitmap_bottom = 1 << 3,
  bitmap_align_mask = bitmap_left | bitmap_right | bitmap_top | bitmap_bottom,
  
  bitmap_line = 1 << 4,
  bitmap_fill = 1 << 5,
  bitmap_primitive_mask = bitmap_line | bitmap_fill,
  
  bitmap_clear = 0 << 6,
  bitmap_draw = 1 << 6,
  bitmap_invert = 2 << 6,
  bitmap_operation_mask = bitmap_clear | bitmap_draw | bitmap_invert,
} bitmap_flags_t;

/**
 * @brief Bitmap font
 */
typedef struct {
  /**
   * @brief psf1 font data
   */
  const bitmap_cell_t *data;
  /**
   * @brief psf1 font size
   */
  bitmap_size_t size;
} bitmap_font_t;

/**
 * @brief Bitmap drawing state
 */
typedef struct {
#if BITMAP_USE_AFFINE
  /**
   * @brief Current transformation state
   */
  bitmap_matrix_t trans;
#endif
  bitmap_coord_t cols;
  bitmap_coord_t rows;

  /**
   * @brief Operation flags
   */
  bitmap_flags_t flags;

  /**
   * @brief Font for text rendering
   */
  bitmap_font_t font;
} bitmap_state_t;

/**
 * @brief Bitmap canvas config
 */
typedef struct {
  bitmap_cell_t *frame;
  bitmap_bound_t bound;
  bitmap_state_t *state;
} bitmap_canvas_t;

void bitmap_init(const bitmap_canvas_t *const c);

static inline void bitmap_flags(const bitmap_canvas_t *const c, bitmap_flags_t f) {
  c->state->flags = f;
}

static inline void bitmap_align(const bitmap_canvas_t *const c, bitmap_flags_t f) {
  c->state->flags = (c->state->flags & ~bitmap_align_mask) | (f & bitmap_align_mask);
}

static inline void bitmap_primitive(const bitmap_canvas_t *const c, bitmap_flags_t f) {
  c->state->flags = (c->state->flags & ~bitmap_primitive_mask) | (f & bitmap_primitive_mask);
}

static inline void bitmap_operation(const bitmap_canvas_t *const c, bitmap_flags_t f) {
  c->state->flags = (c->state->flags & ~bitmap_operation_mask) | (f & bitmap_operation_mask);
}

#if BITMAP_USE_AFFINE
extern const bitmap_matrix_t bitmap_identity;

static inline void bitmap_set_transform(const bitmap_canvas_t *const c, const bitmap_matrix_t *m) {
  c->state->trans = *m;
}

void bitmap_transform(const bitmap_canvas_t *const c, const bitmap_matrix_t *m);
#endif

#define bitmap_numof(x) (sizeof(x)/sizeof(x[0]))

void bitmap_apply(const bitmap_canvas_t *const c);

void bitmap_rect(const bitmap_canvas_t *const c,
                 bitmap_coord_t x, bitmap_coord_t y,
                 bitmap_coord_t X, bitmap_coord_t Y);

void bitmap_circle(const bitmap_canvas_t *const c,
                   bitmap_coord_t X, bitmap_coord_t Y,
                   bitmap_coord_t R);

void bitmap_polygon(const bitmap_canvas_t *const c,
                    const bitmap_point_t *p,
                    bitmap_size_t l);

uint32_t bitmap_strlen(const char *text);

void bitmap_text(const bitmap_canvas_t *const c,
                 bitmap_coord_t x, bitmap_coord_t y,
                 bitmap_coord_t X, bitmap_coord_t Y,
                 const char *text);

#define bitmap_font(c, name)             \
  extern bitmap_cell_t name##_psf_start; \
  extern bitmap_cell_t name##_psf_end;   \
  bitmap_set_font(c, &name##_psf_start,  \
                  &name##_psf_end -      \
                  &name##_psf_start)

static inline void bitmap_set_font(const bitmap_canvas_t *const c,
                                   const bitmap_cell_t *data,
                                   bitmap_size_t size) {
  c->state->font.data = data;
  c->state->font.size = size;
}

void bitmap_glyph_size(const bitmap_canvas_t *const c,
                       bitmap_coord_t *w,
                       bitmap_coord_t *h);

#endif /* BITMAP_H */
