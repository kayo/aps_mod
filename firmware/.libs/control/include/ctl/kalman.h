#ifndef __CTL__KALMAN_H__
#define __CTL__KALMAN_H__
/**
 * @defgroup kalman Kalman filtering
 * @brief This part implements Kalman filtering which also known as linear quadratic estimation (LQE).
 *
 * @{
 */

#include "ctl/type.h"

/**
 * @brief Filter parameters type
 *
 * @param[in] type The type of values
 * @return The filter parameters structure
 */
#define kalman_param_t(type)                                          \
  struct {                                                            \
    /**                                                               \
     * @brief The factor of actual value to previous actual value     \
     */                                                               \
    type F;                                                           \
    /**                                                               \
     * @brief The factor of measured value to actual value            \
     */                                                               \
    type H;                                                           \
    /**                                                               \
     * @brief The measurement noise                                   \
     */                                                               \
    type Q;                                                           \
    /**                                                               \
     * @brief The environment noise                                   \
     */                                                               \
    type R;                                                           \
  }

/**
 * @brief Set factor of actual value to previous actual value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] F The factor value
 */
#define kalman_set_F(type, param, F_)           \
  ((param)->F = (F_))

/**
 * @brief Set factor of measured value to actual value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] H The factor value
 */
#define kalman_set_H(type, param, H_)           \
  ((param)->H = (H_))

/**
 * @brief Set measurement noise
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] H The factor value
 */
#define kalman_set_Q(type, param, Q_)           \
  ((param)->Q = (Q_))

/**
 * @brief Set environment noise
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] R The factor value
 */
#define kalman_set_R(type, param, R_)           \
  ((param)->R = (R_))

/**
 * @brief Filter state type
 *
 * @param[in] type The type of values
 * @return The filter parameters structure
 */
#define kalman_state_t(type) \
  struct {                   \
    /**                      \
     * @brief The state      \
     */                      \
    type X;                  \
    /**                      \
     * @brief The covariance \
     */                      \
    type P;                  \
  }

/**
 * @brief Initialize filter state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to filter state
 * @param[in] X The initial source value
 * @param[in] P The initial covariance
 */
#define kalman_init(type, state, X_, P_) \
  ({                                     \
    (state)->P = (P_);                   \
    (state)->X = (X_);                   \
  })

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] X The source value
 * @return The filtered value
 */
#define kalman_step(type, param, state, X_)             \
  ({                                                    \
    /* prediction: */                                   \
    /* predicted state */                               \
    /* X0 = param.F * state.X */                        \
    type X0 = math_op(type, mul,                        \
                      (param)->F,                       \
                      (state)->X);                      \
    /* predicted covariance */                          \
    /* P0 = param.F^2 * state.P + param.Q */            \
    type P0 = math_op(type, add,                        \
                      math_op(type, mul,                \
                              math_op(type, mul,        \
                                      (param)->F,       \
                                      (param)->F),      \
                              (state)->P),              \
                      (param)->Q);                      \
    /* correction: */                                   \
    /* K = H * P0 / (param.H^2 * P0 + R) */             \
    type K =                                            \
      math_op(type, div,                                \
              math_op(type, mul, (param)->H, P0),       \
              math_op(type, add,                        \
                      math_op(type, mul,                \
                              math_op(type, mul,        \
                                      (param)->H,       \
                                      (param)->H),      \
                              P0),                      \
                      (param)->R));                     \
    /* P = (1 - K * param.H) * P0 */                    \
    (state)->P =                                        \
      math_op(type, mul,                                \
              math_op(type, sub,                        \
                      math_op(type, make, 1.0),         \
                      math_op(type, mul, K,             \
                              (param)->H)),             \
              P0);                                      \
    /* X = X0 + K * (X - param.H * X0) */               \
    (state)->X =                                        \
      math_op(type, add, X0,                            \
              math_op(type, mul, K,                     \
                      math_op(type, sub, X_,            \
                              math_op(type, mul,        \
                                      (param)->H,       \
                                      X0))));           \
  })

/**
 * @}
 */
#endif /* __CTL__KALMAN_H__ */
