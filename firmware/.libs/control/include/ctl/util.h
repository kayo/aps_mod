#ifndef __CTL__UTIL_H__
#define __CTL__UTIL_H__
/**
 * @defgroup util Utilities
 * @brief This part implements some utility macros.
 *
 * @{
 */

#include "ctl/type.h"

/**
 * @brief Estimate actual error using target and actual value
 *
 * @param[in] type The type of values
 * @param[in] target The target value
 * @param[in] actual The actual value
 * @return error The error value
 */
#define error_val(type, target, actual)         \
  math_op(type, sub, actual, target)

/**
 * @brief Get minimum value of two different values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 */
#define min_val(type, val1, val2)                   \
  (math_op(type, lt, val1, val2) ? (val1) : (val2))

/**
 * @brief Get maximum value of two different values
 *
 * @param[in] type The type of values
 * @param[in] val1 The first value
 * @param[in] val2 The second value
 * @return The result value
 *
 * If required the both values must be prescaled to common range.
 */
#define max_val(type, val1, val2)                   \
  (math_op(type, lt, val1, val2) ? (val2) : (val1))

/**
 * @brief Reduce value to range
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] min The minimum value
 * @param[in] max The maximum value
 * @return The result value
 */
#define clamp_val(type, val, min, max)          \
  (math_op(type, lt, val, min) ? (min) :        \
   math_op(type, lt, max, val) ? (max) : (val))

/**
 * @brief Reduce value to range
 *
 * Constant range version
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] min The minimum value
 * @param[in] max The maximum value
 * @return The result value
 */
#define clamp_val_const(type, val, min, max)    \
  clamp_val(type, val,                          \
            math_op(type, make, min),           \
            math_op(type, make, max))

/**
 * @brief Re-scale value from range [a, b] to range [l, h]
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] a The minimum value of source range
 * @param[in] b The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @return The result value
 */
#define scale_val(type, val, a, b, l, h)                     \
  math_op(type, add, l,                                      \
          math_op(type, mul,                                 \
                  math_op(type, sub, val, a),                \
                  math_op(type, div,                         \
                          math_op(type, sub, h, l),          \
                          math_op(type, sub, b, a))))

/**
 * @brief Re-scale value from range [a, b] to range [l, h]
 *
 * Constant range version
 *
 * @param[in] type The type of values
 * @param[in] val The source value
 * @param[in] a The minimum value of source range
 * @param[in] b The maximum value of source range
 * @param[in] l The minimum value of target range
 * @param[in] h The maximum value of target range
 * @return The result value
 */
#define scale_val_const(type, val, a, b, l, h)               \
  math_op(type, add,                                         \
          math_op(type, mul,                                 \
                  math_op(type, sub, val,                    \
                          math_op(type, make, (float)(a))),  \
                  math_op(type, make,                        \
                          ((float)(h) - (float)(l)) /        \
                          ((float)(b) - (float)(a)))),       \
          math_op(type, make, (float)(l)))

/**
 * @}
 */
#endif /* __CTL__UTIL_H__ */
