#ifndef __CTL__APERLN_H__
#define __CTL__APERLN_H__
/**
 * @defgroup aperln Aperiodic link filtering
 * @brief This part implements first order aperiodic link filtering also known as PT1-Glied.
 *
 * @{
 */

#include "ctl/type.h"

/**
 * @brief Aperiodic link parameters type
 *
 * @param[in] type The type of values
 * @return The parameters structure
 */
#define aperln_param_t(type)             \
  struct {                               \
    /**                                  \
     * @brief The time factor of filter  \
     *                                   \
     * 1.0 / (1.0 + Tf/Ts)               \
     */                                  \
    type Kx;                             \
    /**                                  \
     * @brief The time factor of filter  \
     *                                   \
     * (Tf/Ts) / (1.0 + Tf/Ts)           \
     */                                  \
    type _Kx;                            \
  }

/**
 * @brief Set time factor of filter
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] time The time factor value
 * @param[in] period The sampling time (step period)
 */
#define aperln_set_T(type, param, time, period)  \
  ({                                             \
    const type T =                               \
      math_op(type, div, time, period);          \
    const type T1 =                              \
      math_op(type, add, T,                      \
              math_op(type, make, 1.0));         \
    (param)->Kx =                                \
      math_op(type, div,                         \
              math_op(type, make, 1.0), T1);     \
    (param)->_Kx =                               \
      math_op(type, div, T, T1);                 \
    T;                                           \
  })

/**
 * @brief Aperiodic link state type
 *
 * @param[in] type The type of values
 * @return The state structure
 */
#define aperln_state_t(type)               \
  struct {                                 \
    /**                                    \
     * @brief The previous filtered value  \
     */                                    \
    type X;                                \
  }

/**
 * @brief Initialize filter state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to filter state
 * @param[in] value The initial source value
 */
#define aperln_init(type, state, value)         \
  ((state)->X = (value))

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] value The source value
 * @return The filtered value
 */
#define aperln_step(type, param, state, value)   \
  /* state.X = (1.0 * X + (param.T * state.X))   \
     / (1.0 + param.T)                           \
     state.X = X * (1.0 / (1.0 + param.T))       \
     + state.X * (param.T / (1.0 + param.T)) */  \
  ((state)->X =                                  \
   math_op(type, add,                            \
           math_op(type, mul, (value),           \
                   (param)->Kx),                 \
           math_op(type, mul, (state)->X,        \
                   (param)->_Kx)))

/**
 * @}
 */
#endif /* __CTL__APERLN_H__ */
