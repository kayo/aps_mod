#ifndef __CTL__TYPE_H__
#define __CTL__TYPE_H__
/**
 * @defgroup type Types
 * @brief This part implements generic numeric types and math operations.
 *
 * @{
 */

#include <stdint.h>

#define _math_op(type, op) type##_##op

/**
 * @brief Apply generic operation
 *
 * @param[in] type The type of values
 * @param[in] op The name of operation
 * @param[in] ... The operands
 * @return The result of operation
 *
 * Currently supported types:
 *
 * @li float - the platform native floating point number
 * @li fix8 - the 32-bit fixed point number with 8 bits in fraction part
 * @li fix16 - the 32-bit fixed point number with 16 bits in fraction part
 * @li fix24 - the 32-bit fixed point number with 24 bits in fraction part
 *
 * Currently supported operations:
 *
 * @li float - convert value to float
 * @li int - convert value to int
 * @li make - make value from float or int
 * @li add - add two values
 * @li sub - substract two values
 * @li mul - multiply two values
 * @li div - divide two values
 * @li lt - compare two values
 */
#define math_op(type, op, ...) _math_op(type, op)(__VA_ARGS__)

#define float_float(a) (a)
#define float_int(a) ((int32_t)(a))
#define float_make(a) (a)
#define float_add(a, b) ((a) + (b))
#define float_sub(a, b) ((a) - (b))
#define float_mul(a, b) ((a) * (b))
#define float_div(a, b) ((a) / (b))
#define float_lt(a, b) ((a) < (b))

typedef int32_t fixed;
typedef int64_t dfixed;

#define fixed_float(a, q) ((a) / (float)(1 << (q)))
#define fixed_int(a, q) ((a) >> (q))
#define fixed_make(a, q) ((fixed)((a) * (1 << (q))))
#define fixed_add(a, b) ((a) + (b))
#define fixed_sub(a, b) ((a) - (b))
#define fixed_mul(a, b, q) ((fixed)(((dfixed)(a) * (dfixed)(b)) >> (q)))
#define fixed_div(a, b, q) ((fixed)(((dfixed)(a) << (q)) / (dfixed)(b)))
#define fixed_lt(a, b) ((a) < (b))

typedef int32_t fix8;

#define fix8_float(a) fixed_float(a, 8)
#define fix8_int(a) fixed_int(a, 8)
#define fix8_make(a) fixed_make(a, 8)
#define fix8_add(a, b) fixed_add(a, b)
#define fix8_sub(a, b) fixed_sub(a, b)
#define fix8_mul(a, b) fixed_mul(a, b, 8)
#define fix8_div(a, b) fixed_div(a, b, 8)
#define fix8_lt(a, b) fixed_lt(a, b)

typedef int32_t fix16;

#define fix16_float(a) fixed_float(a, 16)
#define fix16_int(a) fixed_int(a, 16)
#define fix16_make(a) fixed_make(a, 16)
#define fix16_add(a, b) fixed_add(a, b)
#define fix16_sub(a, b) fixed_sub(a, b)
#define fix16_mul(a, b) fixed_mul(a, b, 16)
#define fix16_div(a, b) fixed_div(a, b, 16)
#define fix16_lt(a, b) fixed_lt(a, b)

typedef int32_t fix24;

#define fix24_float(a) fixed_float(a, 24)
#define fix24_int(a) fixed_int(a, 24)
#define fix24_make(a) fixed_make(a, 24)
#define fix24_add(a, b) fixed_add(a, b)
#define fix24_sub(a, b) fixed_sub(a, b)
#define fix24_mul(a, b) fixed_mul(a, b, 24)
#define fix24_div(a, b) fixed_div(a, b, 24)
#define fix24_lt(a, b) fixed_lt(a, b)

/**
 * @}
 */
#endif /* __CTL__TYPE_H__ */
