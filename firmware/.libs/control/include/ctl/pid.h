#ifndef __CTL__PID_H__
#define __CTL__PID_H__

#include "ctl/type.h"
/**
 * @defgroup pid Proportional Integral Derivative controller
 * @brief This part implements proportional integral derivative controller
 *
 * @{
 */

/**
 * @brief PID parameters type
 *
 * @param[in] type The type of values
 * @return The PID parameters structure
 */
#define pid_param_t(type)                       \
  struct {                                      \
    /**                                         \
     * @brief Proportional factor (Kp)          \
     */                                         \
    type Kp;                                    \
    /**                                         \
     * @brief Integral factor (1/Ti)            \
     */                                         \
    type Ki;                                    \
    /**                                         \
     * @brief Integral error limit              \
     */                                         \
    type Ei_min, Ei_max;                        \
    /**                                         \
     * @brief Derivative factor (1/Kd)          \
     */                                         \
    type Td;                                    \
  }

/**
 * @brief PID state type
 *
 * @param[in] type The type of values
 * @return The PID state structure
 */
#define pid_state_t(type)                               \
  struct {                                              \
    /**                                                 \
     * @brief Last error                                \
     *                                                  \
     * The difference between target and actual values. \
     */                                                 \
    type E;                                             \
    /**                                                 \
     * @brief Integral of error                         \
     *                                                  \
     * The accumulated error.                           \
     */                                                 \
    type Ei;                                            \
  }

/**
 * @brief Set proportional factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] proportional The proportional factor value
 */
#define pid_set_Kp(type, param, proportional)   \
  ((param)->Kp = (proportional))

/**
 * @brief Set integral factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor value
 *
 * The value of integral factor must be represented in time units.
 * The integral factor value depend from sampling time.
 */
#define pid_set_i(type, param, integral)              \
  ((param)->Ki =                                      \
   math_op(type, div,                                 \
           math_op(type, make, 1.0),                  \
           (integral)))

/**
 * @brief Set integral factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] integral The integral factor time
 * @param[in] period The sampling time (control step period)
 *
 * The value of integral factor must be represented in time units.
 */
#define pid_set_Ti(type, param, integral, period)   \
  pid_set_i(type, param,                            \
            math_op(type, mul, integral, period))

/**
 * @brief Limiting integral error to the desired range
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] min The minimum of integral error
 * @param[in] max The maximum of integral error
 */
#define pid_set_Ei_range(type, param, min, max)   \
  ({                                              \
    (param)->Ei_min = (min);                      \
    (param)->Ei_max = (max);                      \
  })

/**
 * @brief Limiting integral error to the absolute maximum
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] max The absolute maximum of integral error
 */
#define pid_set_Ei_limit(type, param, max)      \
  pid_set_Ei_range(type, param,                 \
                   math_op(type, sub, 0, max),  \
                   max)

/**
 * @brief Set derivative factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 *
 * The value of derivative factor must be represented in time units.
 * The derivative factor value depend from sampling time.
 */
#define pid_set_d(type, param, derivative) \
  ((param)->Td = (derivative))

/**
 * @brief Set derivative factor value
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to PID parameters
 * @param[in] derivative The derivative factor value
 * @param[in] period The sampling time (control step period)
 *
 * The value of derivative factor must be represented in time units.
 */
#define pid_set_Td(type, param, derivative, period)   \
  pid_set_d(type, param,                              \
            math_op(type, mul, derivative, period))

/**
 * @brief Initialize PID state with zeros
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to PID state
 */
#define pid_init(type, state) ({                \
      /* state.E = 0 */                         \
      (state)->E = math_op(type, make, 0.0);    \
      /* state.Ei = 0 */                        \
      (state)->Ei = math_op(type, make, 0.0);   \
      (state);                                  \
    })

/**
 * @brief Evaluate PID control step
 *
 * @param[in] type The type of values
 * @param[in] param The pointer to PID parameters
 * @param[in,out] state The pointer to PID state
 * @param[in] error The actual error value
 * @return The result control value
 */
#define pid_step(type, param, state, error) ({                          \
      /* Ed = error - state.E */                                        \
      type Ed = math_op(type, sub, (error), (state)->E);                \
      /* state.Ei += error */                                           \
      (state)->Ei = clamp_val(type,                                     \
                              math_op(type, add, (state)->Ei, (error)), \
                              (param)->Ei_min,                          \
                              (param)->Ei_max);                         \
      /* state.E = error */                                             \
      (state)->E = (error);                                             \
      /* (error + state.Ei * param.Ki + Ed * param.Td) * param.Kp */    \
      math_op(type, mul,                                                \
              math_op(type, add, (error),                               \
                      math_op(type, add,                                \
                              math_op(type, mul, (state)->Ei,           \
                                      (param)->Ki),                     \
                              math_op(type, mul, Ed, (param)->Td))),    \
              (param)->Kp);                                             \
    })

/**
 * @}
 */
#endif /* __CTL__PID_H__ */
