#ifndef __CTL__EWMA_H__
#define __CTL__EWMA_H__
/**
 * @defgroup ewma Exponentially Weighted Moving Average
 * @brief This part implements exponentially weighted moving average filtering.
 *
 * @{
 */

#include "ctl/type.h"

/**
 * @brief Filter parameters type
 *
 * @param[in] type The type of values
 * @return The filter parameters structure
 */
#define ewma_param_t(type)                      \
  struct {                                      \
    /**                                         \
     * @brief The value of lambda parameter     \
     */                                         \
    type L;                                     \
    /**                                         \
     * @brief The value of 1-lambda parameter   \
     */                                         \
    type _L;                                    \
  }

/**
 * @brief Get const EWMA parameters using lambda factor
 *
 * @param[in] type The type of values
 * @param[in] lambda The value of lambda factor
 * @return The filter parameters constant
 *
 * Usually the lambda factor can be treated as the weight of actual value in result.
 * This meaning that when the lambda equals to 1 then no smoothing will be applied.
 * The less lambda does more smoothing and vise versa.
 */
#define ewma_make_lambda(type, lambda) {        \
    (lambda),                                   \
    math_op(type, sub,                          \
            math_op(type, make, 1.0), (lambda)) \
  }

/**
 * @brief Get const EWMA parameters using N factor
 *
 * @param[in] type The type of values
 * @param[in] N The value of N factor [1...]
 * @return The filter parameters constant
 *
 * Usually the N factor can be treated as the number of steps for smoothing.
 * This meaning that when the N equals to 1 then no smoothing will be applied.
 * The more N does more smoothing and vise versa.
 */
#define ewma_make_N(type, N)                              \
  ewma_make_lambda(type,                                  \
                   math_op(type, div,                     \
                           math_op(type, make, 2.0),      \
                           math_op(type, make, (N) + 1)))

/**
 * @brief Initialize EWMA parameters using lambda factor
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] lambda The value of lambda factor
 *
 * Usually the lambda factor can be treated as the weight of actual value in result.
 * This meaning that when the lambda equals to 1 then no smoothing will be applied.
 * The less lambda does more smoothing and vise versa.
 */
#define ewma_set_lambda(type, param, lambda_) ({      \
      (param)->L = lambda_;                           \
      (param)->_L = math_op(type, sub,                \
                            math_op(type, make, 1.0), \
                            (param)->L);              \
      (param)->L;                                     \
    })

/**
 * @brief Initialize EWMA parameters using N factor
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] N The value of N factor [1...]
 *
 * Usually the N factor can be treated as the number of steps for smoothing.
 * This meaning that when the N equals to 1 then no smoothing will be applied.
 * The more N does more smoothing and vise versa.
 */
#define ewma_set_N(type, param, N)                       \
  /* lambda = 2 / (N + 1)                                \
     N = 1 => lambda = 1                                 \
     N > 1 => lambda < 1 */                              \
  ewma_set_lambda(type, param,                           \
                  math_op(type, div,                     \
                          math_op(type, make, 2.0),      \
                          math_op(type, make, (N) + 1)))

/**
 * @brief Initialize EWMA parameters using time factor
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in] time The smooth time value
 * @param[in] period The sampling time (control step period)
 *
 */
#define ewma_set_T(type, param, time, period)             \
  /* lambda = 2 / (time / period + 1)                     \
     time = period => lambda = 1                          \
     time > period => lambda < 1 */                       \
  ewma_set_lambda(type, param,                            \
                  math_op(type, div,                      \
                          math_op(type, make, 2.0),       \
                          math_op(type, add,              \
                                  math_op(type, div,      \
                                          time, period),  \
                                  math_op(type, make,     \
                                          1.0))))

/**
 * @brief Filter state type
 *
 * @param[in] type The type of values
 * @return The filter state structure
 */
#define ewma_state_t(type)                      \
  struct {                                      \
    /**                                         \
     * @brief The last value                    \
     */                                         \
    type X;                                     \
  }

/**
 * @brief Initialize filter state
 *
 * @param[in] type The type of values
 * @param[in,out] state The pointer to filter state
 * @param[in] value The initial value
 */
#define ewma_init(type, state, value)           \
  ((state)->X = (value))

/**
 * @brief Evaluate filtering step
 *
 * @param[in] type The type of values
 * @param[in,out] param The pointer to filter parameters
 * @param[in,out] state The pointer to filter state
 * @param[in] value The source value
 * @return The filtered value
 */
#define ewma_step(type, param, state, value)             \
  /* X = lambda * value + (1 - lambda) * state.X */      \
  ((state)->X =                                          \
   math_op(type, add,                                    \
           math_op(type, mul, (param)->L, (value)),      \
           math_op(type, mul, (param)->_L, (state)->X)))

/**
 * @}
 */
#endif /* __CTL__EWMA_H__ */
