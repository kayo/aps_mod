#include "ctl/pid.h"

void pid_set_Ti(pid_param_t *param, pid_value_t Ti) {
  //param->Ki = 1.0 / (Ti * param->T);
  param->Ki = fp_div(fp_make(1.0), fp_mul(Ti, param->T));
}

void pid_set_Td(pid_param_t *param, pid_value_t Td) {
  //param->Td = Td * param->T;
  param->Td = fp_mul(Td, param->T);
}

void pid_init(pid_state_t *state) {
  //state->E = 0.0;
  //state->Ei = 0.0;
  state->E = fp_make(0.0);
  state->Ei = fp_make(0.0);
}

pid_value_t pid_step(const pid_param_t *param, pid_state_t *state, pid_value_t E) {
  /*
  pid_value_t Ed = E - state->E;
  state->Ei += E;
  state->E = E;
  
  return (E + state->Ei * param->Ki + Ed * param->Td) * param->Kp;
  */
  pid_value_t Ed = fp_sub(E, state->E);
  state->Ei = fp_add(state->Ei, E);
  state->E = E;

  return fp_mul(fp_add(E, fp_add(fp_mul(state->Ei, param->Ki), fp_mul(Ed, param->Td))), param->Kp);
}
