#include "ctl/util.h"
#include "ctl/pid.h"

int main() {
  { /* bench pid */
    delay_init();
    
    pid_param_t(float) pid_param;
    
    pid_set_Kp(float, &pid_param, float_make(0.5));
    pid_set_Ti(float, &pid_param, float_make(1.0), float_make(100e-3));
    pid_set_Td(float, &pid_param, float_make(0.7), float_make(100e-3));
    
    pid_state_t(float) pid_state;
    pid_init(float, &pid_state);

    delay_t s = delay_snap();
    delay_t m = delay_snap() - s;
    float value = float_make(0.123456);
    volatile float result;
    s = delay_snap();
    for (; float_lt(value, float_make(30.0)); value = float_add(value, float_make(0.1))) {
      result = pid_step(float, &pid_param, &pid_state, error_val(float, float_make(12.0), value));
    }
    delay_t e = delay_snap();
    volatile delay_t d = e - s - m;
  }
  
  { /* bench pid */
    delay_init();
    
    pid_param_t(fix16) pid_param;
    
    pid_set_Kp(fix16, &pid_param, fix16_make(0.5));
    pid_set_Ti(fix16, &pid_param, fix16_make(1.0), fix16_make(100e-3));
    pid_set_Td(fix16, &pid_param, fix16_make(0.7), fix16_make(100e-3));
    
    pid_state_t(fix16) pid_state;
    pid_init(fix16, &pid_state);

    delay_t s = delay_snap();
    delay_t m = delay_snap() - s;
    fix16 value = fix16_make(0.123456);
    volatile fix16 result;
    s = delay_snap();
    for (; fix16_lt(value, fix16_make(30.0)); value = fix16_add(value, fix16_make(0.1))) {
      result = pid_step(fix16, &pid_param, &pid_state, error_val(fix16, fix16_make(12.0), value));
    }
    delay_t e = delay_snap();
    volatile delay_t d = e - s - m;
  }
}
