libqfp.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libqfp
libqfp.INHERIT := firmware
libqfp.CDIRS := $(libqfp.BASEPATH)$(libqfp.target)
libqfp.SRCS := $(libqfp.BASEPATH)$(libqfp.target)/qfplib.S
