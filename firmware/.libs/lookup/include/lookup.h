#ifndef __LOOKUP_H__
#define __LOOKUP_H__ "lookup.h"

#include "ctl/type.h"
#include "ctl/util.h"

#define lookup_table_file(name) LOOKUP_TABLE_##name##_H

#define lookup_table(type, name, func, from, step)    \
  static const type name##_data[] = {                 \
    LOOKUP_TABLE_##name##_DATA(func)                  \
  };                                                  \
  static inline type name(type arg) {                 \
    arg = math_op(type, sub, arg, from);              \
    int idx = math_op(type, int,                      \
                      math_op(type, div, arg, step)); \
    if (idx < 0) {                                    \
      idx = 0;                                        \
    } else if ((unsigned)idx > sizeof(name##_data) /  \
               sizeof(name##_data[0]) - 2) {          \
      idx = sizeof(name##_data) /                     \
        sizeof(name##_data[0]) - 2;                   \
    }                                                 \
    arg = math_op(type, sub, arg,                     \
                  math_op(type, mul, step,            \
                          math_op(type, make, idx))); \
    return                                            \
      math_op(type, add, name##_data[idx],            \
              math_op(type, mul,                      \
                      math_op(type, mul, arg,         \
                              math_op(type, div,      \
                                      math_op(type,   \
                                              make,   \
                                              1.0),   \
                                      step)),         \
                      math_op(type, sub,              \
                              name##_data[idx+1],     \
                              name##_data[idx])));    \
  }

#endif /* __LOOKUP_H__ */
