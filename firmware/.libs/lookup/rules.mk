lookup.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))
lookup.CDIRS := $(lookup.BASEPATH)include

include $(lookup.BASEPATH)lookup.mk

lookup.GENDIR ?= gen/lookup
lookup.GEN_H = $(1).lt.h
lookup.GEN_P = $(lookup.GENDIR)/$(call lookup.GEN_H,$(1))
lookup.CDIRS += $(lookup.GENDIR)

$(lookup.GENDIR)/:
	$(Q)mkdir -p $@

define LTG-RULES
# $1    $2     $3     $4
# <lib> <name> <size> <dependents...>
$$(call lookup-gen,$$(call lookup.GEN_P,$(2)),$(2),$(3),$(4))
$(1).CDEFS += LOOKUP_TABLE_$(2)_H='"$$(call lookup.GEN_H,$(2))"'
$(4): $$(call lookup.GEN_P,$(2))
clean.lookup.$(1): clean.lookup.$(1).$(2)
clean.lookup.$(1).$(2):
	@echo TARGET $(1) LOOKUP $(2) CLEAN
	$(Q)rm -f $$(call lookup.GEN_P,$(2))
endef

ltg-rules = $(eval $(call LTG-RULES,$(1),$(word 1,$(2)),$(word 2,$(2)),$(3)))

# lookup table generating rules
define LTG_RULES
ifneq (,$$($(1).LTGS))
$(1).INHERIT += lookup
$$(foreach t,$$($(1).LTGS),$$(call ltg-rules,$(1),$$(subst :, ,$$(t)),$$($(1).SRCS)))
clean.lookup: clean.lookup.$(1)
endif
endef

clean: clean.lookup
clean.lookup:
