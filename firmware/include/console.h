#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#define console_fn_(instance, function) instance##_##function
#define console_fn(instance, function, ...) console_fn_(instance, function)(__VA_ARGS__)

#define console_def(instance)                 \
  void console_fn(instance, init, void);      \
  void console_fn(instance, done, void);      \
  void console_fn(instance, step,             \
                  const param_coll_t *params)

#endif /* __CONSOLE_H__ */
