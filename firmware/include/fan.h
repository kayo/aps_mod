#ifndef __FAN_H__
#define __FAN_H__

#include "ctl/pid.h"

#define fan_pid_param_s pid_param_t(fix16)
typedef fan_pid_param_s fan_pid_param_t;

extern fan_pid_param_t Tpid_param;

void fan_init(void);
void fan_done(void);
void fan_step(void);

#endif /* __FAN_H__ */
