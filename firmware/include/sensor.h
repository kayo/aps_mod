#ifndef __SENSER_H__
#define __SENSOR_H__

#include "ctl/ewma.h"
#include "ctl/aperln.h"

#include "macro.h"

#define temp_filter_param_s _CAT2(TEMP_FILTER, _param_t)(fix16)
typedef temp_filter_param_s temp_filter_param_t;
extern temp_filter_param_t temp_filter_param;

#define curr_filter_param_s _CAT2(CURR_FILTER, _param_t)(fix16)
typedef curr_filter_param_s curr_filter_param_t;
extern curr_filter_param_t curr_filter_param;

#define volt_filter_param_s _CAT2(VOLT_FILTER, _param_t)(fix16)
typedef volt_filter_param_s volt_filter_param_t;
extern volt_filter_param_t volt_filter_param;

void sensor_init(void);
void sensor_done(void);
extern void sensor_step(void);

#endif /* __SENSOR_H__ */
