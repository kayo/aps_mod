#ifndef __PARAMS_H__
#define __PARAMS_H__

#include "ctl/type.h"

enum {
  state_off,
  state_on,
};

#include PARAMS_params_H

#endif /* __PARAMS_H__ */
