#ifndef __DRIVER_H__
#define __DRIVER_H__

#include "ctl/pid.h"

#define driver_pid_param_s pid_param_t(fix16)
typedef driver_pid_param_s driver_pid_param_t;

extern driver_pid_param_t Ipid_param;
extern driver_pid_param_t Upid_param;

void driver_init(void);
void driver_done(void);
void driver_step(void);

#endif /* __DRIVER_H__ */
