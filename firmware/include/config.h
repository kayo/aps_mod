/* common config */
#define mcu_frequency 24 /* MHz */
#define systick_config 8,100 /* divider(1 or 8),interval(mS) */

/* pcd8544 config */
#define pcd8544_spi 2,8 /* device,divider */
#define pcd8544_dma 1,5 /* controller,channel */
#define pcd8544_dc B,14 /* port,pad */
#define pcd8544_res A,8 /* port,pad */
//#define pcd8544_cs B,12 /* port,pad */
#define pcd8544_bl_pwm 4,4 /* timer,channel */
//#define pcd8544_bl B,9 /* port,pad */
#define bitmap_config 0,0,84,48 /* left,top,right,bottom */

/* encoder handler */
enum {
  evt_enter = 1,
  evt_setup,
  evt_reset,
  evt_incr,
  evt_decr,
  evt_tick,
};

#define btn_config B,2,0,_(evt_enter,100,500)_(evt_setup,1000,3000)_(evt_reset,5000,10000) /* port,pad,active level,_(event,interval mS)... */
#define enc_config (B,0,0),(B,1,0),evt_incr,evt_decr /* (port A,pad A,active level A),(port B,pad B,active level B),cw event,ccw event */

/* pcd8544 backlight pwm */
#define pwm1_config 4,0,1<<12,(nc,nc,nc,pp) /* timer,prescaler,period,(channels configs),remap */

#define sensor_adc 1,12,2,(1.2,3.3) /* adc device,adc bits,adc prescaler,(adc Vref,adc Vtop) */
#define sensor_dma 1,1 /* device,channel */
#define sensor_timer 2,2,10 /* trigger:timer,channel,interval(mS) */
#define sensor_testout A,15 /* port,pad */
#define temp1_input A,4,4 /* port,pad,channel */
#define temp2_input A,5,5 /* port,pad,channel */
#define volt_input A,6,6 /* port,pad,channel */
#define curr_input A,7,7 /* port,pad,channel */

//#define filter_config 16,8 /* average samples,median samples */

#define vref_config /* enable */
#define tmcu_config 25,1.41,4.3 /* T0,Vsense(T0),Avg Slope */

//#define therm_config (4.7e3,0),SH,(0.0013081992342927878,0.00005479611105636211,9.415113523239857e-7) /* resistors(r1,r2),model,params(a,b,c) */
#define therm_config (4.7e3,0),BETA,(3450,100e3,C2K(24)) /* resistors(r1,r2),model,params(beta,r0,t0) */

/* half bridge driver */
#define driver_pwm 1,DRV_FREQ,DRV_DT /* timer,frequency(KHz),dead time(uS) */
#define hikey_output A,9,1,2 /* port,pad,active level,pwm channel */
#define lokey_output A,10,1,3 /* port,pad,active level,pwm channel */

/* driver and fan pwm */
#define fan_timer 1,DRV_FREQ,0 /* timer,frequency,need initialize */
#define fan_output A,11,1,4 /* port,pad,active level,pwm channel */

#define rs485_gpio (A,3),(A,2),rs485e,(A,0),(A,1) /* rx(port,pad,af),tx(port,pad,af),mode,de(port,pad,af),~re(port,pad,af) */
#define rs485_uart 2,115200,8,1 /* uart,baudrate,databits,stopbits */
#define rs485_dma 1,(6,512),(7,512) /* dma,rx(channel,buffer length),tx(channel,buffer length) */
#define rs485_proto json /* json|bin */

#define radio_gpio (B,11),(B,10),none /* rx(port,pad,af),tx(port,pad,af),mode */
#define radio_uart 3,115200,8,1 /* uart,baudrate,databits,stopbits */
#define radio_dma 1,(3,512),(2,512) /* dma,rx(channel,buffer length),tx(channel,buffer length) */
#define radio_proto json /* json|bin */

#define spi2_remap /* no remap */
#define tim4_remap /* no remap */
#define uart2_remap /* no remap */
#define uart3_remap /* no remap */
